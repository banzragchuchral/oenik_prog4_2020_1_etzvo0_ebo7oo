﻿// <copyright file="MainMenu.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Logic;

    /// <summary>
    /// Interaction logic for MainMenu.xaml.
    /// </summary>
    public partial class MainMenu : Window
    {
        private ViewModel viewmodel;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenu"/> class.
        /// </summary>
        public MainMenu()
        {
            this.InitializeComponent();

            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.background.jpg");
            bmp.EndInit();
            ImageBrush myBrush = new ImageBrush(bmp);
            this.Background = myBrush;
            RepositoryLogic repositoryLogic = new RepositoryLogic();
            if (!repositoryLogic.GetboolSave())
            {
                this.LoadButton.IsEnabled = false;
                this.LoadButton.Style = (Style)Application.Current.Resources["RoundButtonTemplateDisabled"];
            }

            this.viewmodel = new ViewModel();
            this.DataContext = this.viewmodel;
        }

        /// <summary>
        /// Refreshload.
        /// </summary>
        public void RefreshLoad()
        {
            RepositoryLogic repositoryLogic = new RepositoryLogic();
            if (!repositoryLogic.GetboolSave())
            {
                this.LoadButton.IsEnabled = false;
                this.LoadButton.Style = (Style)Application.Current.Resources["RoundButtonTemplateDisabled"];
            }
        }
    }
}
