﻿// <copyright file="Gameend.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Logic;

    /// <summary>
    /// Interaction logic for gameend.xaml.
    /// </summary>
    public partial class Gameend : Window
    {
        private double time;

        /// <summary>
        /// Initializes a new instance of the <see cref="Gameend"/> class.
        /// </summary>
        public Gameend()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Gameend"/> class.
        /// </summary>
        /// <param name="timer">timer.</param>
        public Gameend(double timer)
        {
            this.time = timer;
            this.InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ScoreLogicClass<ScoreClass> scoreClass = new ScoreLogicClass<ScoreClass>();
            scoreClass.SaveScore(new ScoreClass(this.nev.Text, this.time));
            this.Close();
        }
    }
}
