﻿// <copyright file="ViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;
    using Logic;

    /// <summary>
    /// Viewmodel class.
    /// </summary>
    public class ViewModel : ViewModelBase
    {
        private int switchview;

        private Logic.ScoreLogicClass<ScoreClass> rep;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModel"/> class.
        /// </summary>
        public ViewModel()
        {
            this.rep = new ScoreLogicClass<ScoreClass>();

            this.Scores = new List<string>();
            this.Tempscore = new List<ScoreClass>();
            if (this.rep.GetScores() != null)
            {
                foreach (string item in this.rep.GetScores())
                {
                    var splits = item.Split(';');
                    this.Tempscore.Add(new ScoreClass(splits[0], Convert.ToDouble(splits[1])));
                }
            }

            this.Tempscore.OrderBy(x => x.Time);
            foreach (var x in this.Tempscore)
            {
                this.Scores.Add(x.Name + ":    " + TimeSpan.FromMinutes(Convert.ToDouble(x.Time)).ToString(@"m\:ss"));
            }

            this.SwitchView = 0;
            this.GameStartCommand = new RelayCommand(() =>
            {
                GameWindow win = new GameWindow();
                win.ShowDialog();
            });
            this.LoadStartCommand = new RelayCommand(() =>
            {
                Application.Current.Resources["Load"] = true;
                GameWindow win = new GameWindow();
                win.ShowDialog();
            });
            this.HighScoreCommand = new RelayCommand(() =>
            {
                Highscore highscore = new Highscore();
                highscore.ShowDialog();
            });
            this.HighScoreCommand = new RelayCommand(() =>
            {
                Highscore highscore = new Highscore();
                highscore.ShowDialog();
            });
            this.ExitCommand = new RelayCommand(() =>
            {
                MainMenu win = (MainMenu)Application.Current.MainWindow;
                win.Close();
            });
        }

        /// <summary>
        /// Gets the start command.
        /// </summary>
        public ICommand GameStartCommand { get; private set; }

        /// <summary>
        /// Gets the menu command.
        /// </summary>
        public ICommand LoadStartCommand { get; private set; }

        /// <summary>
        /// Gets the highscore command.
        /// </summary>
        public ICommand HighScoreCommand { get; private set; }

        /// <summary>
        /// Gets Exitcommand.
        /// </summary>
        public ICommand ExitCommand { get; private set; }

        /// <summary>
        /// Gets or sets list of scores.
        /// </summary>
        public List<string> Scores { get; set; }

        /// <summary>
        /// Gets or sets scoreclass.
        /// </summary>
        public ScoreClass Score { get; set; }

        /// <summary>
        /// Gets or sets switchview.
        /// </summary>
        public int SwitchView
        {
            get { return this.switchview; }
            set { this.Set(ref this.switchview, value); }
        }

        private List<ScoreClass> Tempscore { get; set; }
    }
}
