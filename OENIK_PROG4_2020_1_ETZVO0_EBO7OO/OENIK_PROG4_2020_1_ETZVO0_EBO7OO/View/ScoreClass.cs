﻿// <copyright file="ScoreClass.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View
{
    /// <summary>
    /// Class for scores.
    /// </summary>
    public class ScoreClass
    {
        /// <summary>
        /// Store time.
        /// </summary>
        private double time;

        /// <summary>
        /// Store winner name.
        /// </summary>
        private string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="ScoreClass"/> class.
        /// </summary>
        /// <param name="name_">name.</param>
        /// <param name="time_">time.</param>
        public ScoreClass(string name_, double time_)
        {
            this.time = time_;
            this.name = name_;
        }

        /// <summary>
        /// Gets or sets time.
        /// </summary>
        public double Time { get => this.time; set => this.time = value; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get => this.name; set => this.name = value; }

        /// <summary>
        /// Tostring method.
        /// </summary>
        /// <returns>string.</returns>
        public override string ToString()
        {
            return this.name + ";" + this.time;
        }
    }
}
