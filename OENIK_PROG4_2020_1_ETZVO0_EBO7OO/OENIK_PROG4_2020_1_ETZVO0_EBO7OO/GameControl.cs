// <copyright file="GameControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG4_2020_1_ETZVO0_EBO7OO
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Media;
    using System.Threading;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using Logic;
    using Model;
    using OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View;

    /// <summary>
    /// GameControl class.
    /// </summary>
    public class GameControl : FrameworkElement
    {
        private GameModel model;
        private Renderer renderer;
        private IJatekosLogic playerlogic;
        private IEllensegLogic enemylogic;
        private IMovingBlockLogic movingblockLogic;
        private DispatcherTimer timer;
        private RepositoryLogic replogic;
        private bool aDown;
        private bool dDown;
        private Key downkey;
        private Stopwatch gameTimer;
        private MediaPlayer gameSong;
        private MediaPlayer shootingSound;
        private MediaPlayer deathSound;
        private MediaPlayer hitSound;
        private MediaPlayer jumpSound;
        private MediaPlayer monsterSound;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameControl"/> class.
        /// </summary>
        public GameControl()
        {
            this.Loaded += this.Gameload;
            this.shootingSound = new MediaPlayer();
            this.shootingSound.Open(new Uri(Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "/Songs/shoot.wav"));
            this.shootingSound.MediaEnded += new EventHandler(this.ShootingSoundStop);
            this.deathSound = new MediaPlayer();
            this.deathSound.Open(new Uri(Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "/Songs/deathsound.wav"));
            this.deathSound.MediaEnded += new EventHandler(this.DeathSoundStop);
            this.gameSong = new MediaPlayer();
            this.gameSong.Open(new Uri(Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "/Songs/song.wav"));
            this.gameSong.MediaEnded += new EventHandler(this.LoopSong);
            this.hitSound = new MediaPlayer();
            this.hitSound.Open(new Uri(Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "/Songs/hit.wav"));
            this.hitSound.MediaEnded += new EventHandler(this.HitSoundStop);
            this.jumpSound = new MediaPlayer();
            this.jumpSound.Open(new Uri(Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "/Songs/jump.wav"));
            this.jumpSound.MediaEnded += new EventHandler(this.JumpSoundStop);
            this.monsterSound = new MediaPlayer();
            this.monsterSound.Open(new Uri(Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "/Songs/monsterroar.wav"));
            this.monsterSound.MediaEnded += new EventHandler(this.MonsterSoundStop);
        }

        /// <summary>
        /// Renderer.
        /// </summary>
        /// <param name="drawingContext">drawingcontext.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.renderer != null)
            {
                this.renderer.Drawing(drawingContext);
            }
        }

        private void Gameload(object sender, RoutedEventArgs e)
        {
            GameControl gameControl = this;
            gameControl.gameSong.Play();
            this.replogic = new RepositoryLogic();
            this.gameTimer = new Stopwatch();
            if (Application.Current.Resources["Load"].ToString() == "True")
            {
                this.model = new GameModel(this.ActualWidth, this.ActualHeight, this.replogic.LoadGame());
                Application.Current.Resources["Load"] = false;
            }
            else
            {
                this.model = new GameModel(this.ActualWidth, this.ActualHeight, 1);
            }

            this.playerlogic = new PlayerLogic(this.model);
            this.renderer = new Renderer(this.model, this.gameTimer);
            this.enemylogic = new EnemyLogic(this.model);
            this.movingblockLogic = new MovingBlockLogic(this.model);

            this.aDown = false;
            this.dDown = false;

            GameWindow win = (GameWindow)Window.GetWindow(this);
            if (win != null)
            {
                win.WindowState = WindowState.Maximized;
                win.WindowStyle = WindowStyle.None;
                win.ResizeMode = ResizeMode.NoResize;

                this.timer = new DispatcherTimer();
                this.timer.Interval = TimeSpan.FromMilliseconds(40);
                this.timer.Tick += this.Player_Tick;
                this.timer.Start();

                this.gameTimer.Start();

                win.KeyDown += this.W_KeyDown;
                win.KeyUp += this.W_KeyUp;
            }

            this.enemylogic.PlayerDead += this.Halal;
            this.enemylogic.PlayerDead += this.DeathPlay;
            this.enemylogic.MonsterSound += this.MonsterSoundPlay;
            this.movingblockLogic.PlayerDead += this.Halal;
            this.movingblockLogic.PlayerDead += this.DeathPlay;
            this.movingblockLogic.Enemyhit += this.HitSoundPlay;
            this.playerlogic.EndThisLevel += this.Next;
            this.playerlogic.PlayerDead += this.Halal;
            this.playerlogic.PlayerDead += this.DeathPlay;
            this.playerlogic.Shoot += this.ShootingPlay;
            this.playerlogic.Jump += this.JumpSoundPlay;

            this.InvalidateVisual();
        }

        private void W_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.A))
            {
                this.aDown = false;
            }
            else if (e.Key.Equals(Key.D))
            {
                this.dDown = false;
            }

            this.playerlogic.JatekosMozog(this.aDown, this.dDown);
        }

        private void W_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.D:
                    if (!this.dDown)
                    {
                        this.dDown = true;
                        this.downkey = Key.D;
                        this.playerlogic.JatekosMozog(this.aDown, this.dDown);
                        this.playerlogic.Mozgas(e.Key);
                    }

                    break;
                case Key.A:
                    if (!this.aDown)
                    {
                        this.aDown = true;
                        this.downkey = Key.A;
                        this.playerlogic.JatekosMozog(this.aDown, this.dDown);
                        this.playerlogic.Mozgas(e.Key);
                    }

                    break;
                case Key.W:
                    this.playerlogic.Ugras();
                    break;
                case Key.Space:
                    this.playerlogic.Loves();
                    break;
                case Key.Escape:
                    {
                        this.timer.Stop();
                        this.gameTimer.Stop();
                        Exitmenu exitmenu = new Exitmenu();
                        if (exitmenu.ShowDialog() == false)
                        {
                            if (exitmenu.Ujrakezd)
                            {
                                this.Reset();
                            }
                            else if (exitmenu.Kilep)
                            {
                                this.gameSong.Stop();
                                GameWindow game = (GameWindow)Window.GetWindow(this);

                                MainMenu menu = (MainMenu)Application.Current.MainWindow;
                                menu.RefreshLoad();

                                game.Close();
                            }
                            else if (exitmenu.Mentes)
                            {
                                this.replogic.SaveGame(this.model.GetGame(), this.gameTimer.Elapsed + this.model.SavedTime);
                                this.timer.Start();
                                this.gameTimer.Start();
                            }
                            else
                            {
                                this.gameTimer.Start();
                                this.timer.Start();
                            }
                        }

                        exitmenu.Close();

                        break;
                    }
            }

            this.InvalidateVisual();
        }

        private void Player_Tick(object sender, EventArgs e)
        {
            if (this.model.GameEnds == true)
            {
                this.GameEnd();
            }

            this.playerlogic.AutoMovePlayer();
            if (false)
            {
            }
            else if (this.dDown)
            {
                this.downkey = Key.D;
                this.playerlogic.Mozgas(this.downkey);
            }
            else if (this.aDown)
            {
                this.downkey = Key.A;
                this.playerlogic.Mozgas(this.downkey);
            }

            this.enemylogic.Mozgas();
            if (this.model.Boss != null)
            {
                this.enemylogic.BossAttack();
            }

            this.movingblockLogic.Mozgas();
            this.InvalidateVisual();
        }

        private void Reset()
        {
            this.timer.Stop();
            this.gameTimer = new Stopwatch();
            this.model = new GameModel(this.ActualWidth, this.ActualHeight, 1);
            this.playerlogic = new PlayerLogic(this.model);
            this.renderer = new Renderer(this.model, this.gameTimer);
            this.enemylogic = new EnemyLogic(this.model);
            this.movingblockLogic = new MovingBlockLogic(this.model);
            this.aDown = false;
            this.dDown = false;

            this.timer = new DispatcherTimer();
            this.timer.Interval = TimeSpan.FromMilliseconds(40);
            this.timer.Tick += this.Player_Tick;
            this.timer.Start();
            this.gameTimer.Start();

            this.enemylogic.PlayerDead += this.Halal;
            this.enemylogic.PlayerDead += this.DeathPlay;
            this.enemylogic.MonsterSound += this.MonsterSoundPlay;
            this.movingblockLogic.PlayerDead += this.Halal;
            this.movingblockLogic.PlayerDead += this.DeathPlay;
            this.movingblockLogic.Enemyhit += this.HitSoundPlay;
            this.playerlogic.EndThisLevel += this.Next;
            this.playerlogic.PlayerDead += this.Halal;
            this.playerlogic.PlayerDead += this.DeathPlay;
            this.playerlogic.Shoot += this.ShootingPlay;
            this.playerlogic.Jump += this.JumpSoundPlay;
            this.InvalidateVisual();
        }

        private void Halal()
        {
            int temp = this.model.DeathNumbers;
            this.gameTimer.Stop();
            this.timer.Stop();
            int level = this.model.Level;
            this.model = new GameModel(this.ActualWidth, this.ActualHeight, level);
            this.playerlogic = new PlayerLogic(this.model);
            this.renderer = new Renderer(this.model, this.gameTimer);
            this.enemylogic = new EnemyLogic(this.model);
            this.movingblockLogic = new MovingBlockLogic(this.model);
            this.aDown = false;
            this.dDown = false;
            this.model.DeathNumbers = temp;

            this.timer = new DispatcherTimer();
            this.timer.Interval = TimeSpan.FromMilliseconds(40);
            this.timer.Tick += this.Player_Tick;
            this.timer.Start();

            this.gameTimer.Start();

            this.enemylogic.PlayerDead += this.Halal;
            this.enemylogic.PlayerDead += this.DeathPlay;
            this.enemylogic.MonsterSound += this.MonsterSoundPlay;
            this.movingblockLogic.PlayerDead += this.Halal;
            this.movingblockLogic.PlayerDead += this.DeathPlay;
            this.movingblockLogic.Enemyhit += this.HitSoundPlay;
            this.playerlogic.EndThisLevel += this.Next;
            this.playerlogic.PlayerDead += this.Halal;
            this.playerlogic.PlayerDead += this.DeathPlay;
            this.playerlogic.Shoot += this.ShootingPlay;
            this.playerlogic.Jump += this.JumpSoundPlay;
            this.InvalidateVisual();
        }

        private void Next()
        {
            this.model.KovetkezoMap();

            this.timer.Stop();
            this.playerlogic = new PlayerLogic(this.model);
            this.renderer = new Renderer(this.model, this.gameTimer);
            this.enemylogic = new EnemyLogic(this.model);
            this.movingblockLogic = new MovingBlockLogic(this.model);
            this.aDown = false;
            this.dDown = false;

            this.timer = new DispatcherTimer();
            this.timer.Interval = TimeSpan.FromMilliseconds(40);
            this.timer.Tick += this.Player_Tick;
            this.timer.Start();

            this.enemylogic.PlayerDead += this.Halal;
            this.enemylogic.PlayerDead += this.DeathPlay;
            this.enemylogic.MonsterSound += this.MonsterSoundPlay;
            this.movingblockLogic.PlayerDead += this.Halal;
            this.movingblockLogic.PlayerDead += this.DeathPlay;
            this.movingblockLogic.Enemyhit += this.HitSoundPlay;
            this.playerlogic.EndThisLevel += this.Next;
            this.playerlogic.PlayerDead += this.Halal;
            this.playerlogic.PlayerDead += this.DeathPlay;
            this.playerlogic.Shoot += this.ShootingPlay;
            this.playerlogic.Jump += this.JumpSoundPlay;
            this.InvalidateVisual();
        }

        private void GameEnd()
        {
            this.timer.Stop();
            this.gameTimer.Stop();
            this.gameSong.Stop();
            this.model = new GameModel(this.ActualWidth, this.ActualHeight, 1);
            this.playerlogic = new PlayerLogic(this.model);
            this.renderer = new Renderer(this.model, this.gameTimer);
            this.enemylogic = new EnemyLogic(this.model);
            this.movingblockLogic = new MovingBlockLogic(this.model);
            this.aDown = false;
            this.dDown = false;

            Gameend end = new Gameend(this.gameTimer.Elapsed.TotalMinutes + this.model.SavedTime.TotalMinutes);
            end.ShowDialog();

            Thread.Sleep(10);
            GameWindow win = (GameWindow)Window.GetWindow(this);
            win.Close();
        }

        private void ShootingSoundStop(object sender, EventArgs e)
        {
            this.shootingSound.Stop();
        }

        private void DeathSoundStop(object sender, EventArgs e)
        {
            this.deathSound.Stop();
        }

        private void DeathPlay()
        {
            this.deathSound.Play();
        }

        private void ShootingPlay()
        {
            this.shootingSound.Play();
        }

        private void LoopSong(object sender, EventArgs e)
        {
            this.gameSong.Stop();
            this.gameSong.Play();
        }

        private void HitSoundStop(object sender, EventArgs e)
        {
            this.hitSound.Stop();
        }

        private void HitSoundPlay()
        {
            this.hitSound.Play();
        }

        private void JumpSoundStop(object sender, EventArgs e)
        {
            this.jumpSound.Stop();
        }

        private void JumpSoundPlay()
        {
            this.jumpSound.Play();
        }

        private void MonsterSoundStop(object sender, EventArgs e)
        {
            this.monsterSound.Stop();
        }

        private void MonsterSoundPlay()
        {
            this.monsterSound.Play();
        }
    }
}
