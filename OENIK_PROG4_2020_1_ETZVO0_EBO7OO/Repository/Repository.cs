﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Serialization;
    using Newtonsoft.Json;

    /// <summary>
    /// Holds repository.
    /// </summary>
    public class Repository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// </summary>
        public Repository()
        {
        }

        /// <summary>
        /// Get save.
        /// </summary>
        /// <returns>bool.</returns>
        public bool GetboolSave()
        {
            return File.Exists(@"savegame.xml");
        }

        /// <summary>
        /// Method for saving.
        /// </summary>
        /// <param name="savefile">savefile.</param>
        public void Savegame(List<string> savefile)
        {
            XDocument output = new XDocument(
            new XElement(
                "Root",
                new XElement("Level", savefile[0]),
                new XElement("x", savefile[1]),
                new XElement("y", savefile[2]),
                new XElement("time", savefile[3])));
            using (StreamWriter sw = new StreamWriter(@"savegame.xml"))
            {
                sw.Write(output);
            }
        }

        /// <summary>
        /// Gets the saved game.
        /// </summary>
        /// <returns>string[].</returns>
        public string[] Loadgame()
        {
            XDocument xml = XDocument.Load(@"savegame.xml");
            string[] vissza = new string[4];
            vissza[0] = xml.Element("Root").Element("Level").Value;
            vissza[1] = xml.Element("Root").Element("x").Value;
            vissza[2] = xml.Element("Root").Element("y").Value;
            vissza[3] = xml.Element("Root").Element("time").Value;
            return vissza;
        }
    }
}
