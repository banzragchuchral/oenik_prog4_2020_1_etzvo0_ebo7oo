﻿// <copyright file="HighscoreRepository{T}.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Serialization;
    using Newtonsoft.Json;

    /// <summary>
    /// Higscore repo class.
    /// </summary>
    /// <typeparam name="T">T.</typeparam>
    public class HighscoreRepository<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HighscoreRepository{T}"/> class.
        /// </summary>
        public HighscoreRepository()
        {
            this.Scores = new List<T>();
            this.LoadHighscore();
        }

        /// <summary>
        /// Gets or sets list of highscore.
        /// </summary>
        public List<T> Scores { get; set; }

        /// <summary>
        /// Method for saving score.
        /// </summary>
        /// <param name="asd">score.</param>
        public void SaveHighscore(T asd)
        {
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@"highscore.txt", true))
            {
                file.WriteLine(asd.ToString());
            }
        }

        /// <summary>
        /// Load highscore.
        /// </summary>
        /// <returns>List of string.</returns>
        public List<string> LoadHighscore()
        {
            try
            {
                List<string> vissza = new List<string>();
                using (StreamReader sr = new StreamReader(@"highscore.txt"))
                {
                    while (!sr.EndOfStream)
                    {
                        vissza.Add(sr.ReadLine());
                    }
                }

                return vissza;
            }
            catch (IOException)
            {
                return null;
            }
        }

        /// <summary>
        /// Add highscore.
        /// </summary>
        /// <param name="newscore">newscore.</param>
        public void AddHighscore(T newscore)
        {
            this.SaveHighscore(newscore);
        }
    }
}
