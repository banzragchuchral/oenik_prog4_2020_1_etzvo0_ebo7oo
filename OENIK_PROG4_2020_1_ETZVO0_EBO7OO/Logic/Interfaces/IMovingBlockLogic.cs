﻿// <copyright file="IMovingBlockLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Logic.Interfaces;

    /// <summary>
    /// Delegate for hitting enemy.
    /// </summary>
    public delegate void Hitenemy();

    /// <summary>
    /// Interface for moving block logic.
    /// </summary>
    public interface IMovingBlockLogic : ILogic
    {
        /// <summary>
        /// Event for player dead.
        /// </summary>
        event Hitenemy Enemyhit;

        /// <summary>
        /// Method for moving.
        /// </summary>
        void Mozgas();
    }
}
