﻿// <copyright file="EnemyLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;
    using Logic.Interfaces;
    using Model;
    using Model.Classes;

    /// <summary>
    /// Enemy logic class.
    /// </summary>
    public class EnemyLogic : IEllensegLogic
    {
        private const double MAXRANGE = 250;
        private IGameModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnemyLogic"/> class.
        /// </summary>
        /// <param name="model">model.</param>
        public EnemyLogic(IGameModel model)
        {
            this.model = model;
            this.RangeCreator();
        }

        /// <summary>
        /// Event for player dead.
        /// </summary>
        public event PlayerDeadEventHandler PlayerDead;

        /// <summary>
        /// Event for monster sound.
        /// </summary>
        public event MonsterSoundEvent MonsterSound;

        /// <summary>
        /// Method for moving.
        /// </summary>
        public void Mozgas()
        {
            foreach (var item in this.model.Ellenseg)
            {
                this.ContactWhitPlayer(item);
                if (item.Fordul && this.CanMoveRight(item) && this.StandOnGround(item) && this.model.GetWidth() > item.Area.Right)
                {
                    item.ChangeX(Config.SimpleEnemyMoveSpeed.X);
                    if (this.model.GetWidth() <= item.Area.Right || !this.StandOnGround(item) || item.DistanceTravelled >= item.Range)
                    {
                        item.Fordul = false;
                        item.ChangeX(-Config.SimpleEnemyMoveSpeed.X);
                    }
                }
                else if (!item.Fordul && this.CanMoveLeft(item) && this.StandOnGround(item) && item.Area.Left > 0)
                {
                    item.ChangeX(-Config.SimpleEnemyMoveSpeed.X);
                    if (item.Area.Left <= 0 || !this.StandOnGround(item) || item.DistanceTravelled <= -item.Range)
                    {
                        item.Fordul = true;
                        item.ChangeX(Config.SimpleEnemyMoveSpeed.X);
                    }
                }
            }
        }

        /// <summary>
        /// Method for boss attack.
        /// </summary>
        public void BossAttack()
        {
            if (this.model.Boss.Hand1status == HandStatus.Search || this.model.Boss.Hand2status == HandStatus.Search)
            {
                this.ChooseHand();
            }

            if (this.model.Boss.Hand1status == HandStatus.Wait || this.model.Boss.Hand2status == HandStatus.Wait)
            {
                this.Hand();
            }

            if (this.model.Boss.Hand1status == HandStatus.Hit || this.model.Boss.Hand1status == HandStatus.Endhit ||
                this.model.Boss.Hand2status == HandStatus.Hit || this.model.Boss.Hand2status == HandStatus.Endhit)
            {
                this.HandMove();
            }

            this.StoneBallAttack();
            this.KillPlayer();
            this.BossAttach();

            if (this.model.Boss.Health <= 0)
            {
                this.model.Boss = null;
            }
        }

        private void RangeCreator()
        {
            foreach (var item in this.model.Ellenseg)
            {
                this.Range(item);
            }
        }

        private void Range(IEllenseg ellenseg)
        {
            double range = 0;

            List<IGameBlock> blocks = new List<IGameBlock>();
            foreach (var item in this.model.Gameblocks)
            {
                if (item.Area.Top > ellenseg.Area.Bottom)
                {
                    if (item.Area.Left < ellenseg.Area.Right || item.Area.Right > ellenseg.Area.Left)
                    {
                        blocks.Add(item);
                    }
                }
            }

            double min = 1000000;
            foreach (var item in blocks)
            {
                if (item.Area.Top - ellenseg.Area.Bottom < min)
                {
                    min = item.Area.Top - ellenseg.Area.Bottom;
                }
            }

            blocks = blocks.Where(x => x.Area.Top - ellenseg.Area.Bottom == min).ToList();
            range = this.FullRangeCalculate(blocks);

            if (range != 0)
            {
                ellenseg.Range = this.RealRangeCalculate(range / 2);
            }
            else
            {
                ellenseg.Range = 75;
            }
        }

        private double RealRangeCalculate(double halfFullRange)
        {
            double range = 0;

            if (halfFullRange >= MAXRANGE / 2)
            {
                range = MAXRANGE / 2;
            }
            else
            {
                range = 50;
            }

            return range;
        }

        private double FullRangeCalculate(List<IGameBlock> gameblocks)
        {
            double fullRange = 0;

            if (gameblocks.Count() > 0)
            {
                double minX = gameblocks.OrderBy(x => x.Area.Left).FirstOrDefault().Area.Left;
                double maxX = gameblocks.OrderBy(x => x.Area.Right).LastOrDefault().Area.Right;
                fullRange = maxX - minX;
            }
            else
            {
                fullRange = 75;
            }

            return fullRange;
        }

        private bool CanMoveRight(IEllenseg ellenseg)
        {
            foreach (var item in this.EnemyInteractions(ellenseg))
            {
                if (ellenseg.Area.Bottom > item.Area.Bottom && ellenseg.Area.Right >= item.Area.Left)
                {
                    return false;
                }
            }

            return true;
        }

        private bool CanMoveLeft(IEllenseg ellenseg)
        {
            foreach (var item in this.EnemyInteractions(ellenseg))
            {
                if (ellenseg.Area.Bottom > item.Area.Bottom && ellenseg.Area.Left <= item.Area.Right)
                {
                    return false;
                }
            }

            return true;
        }

        private bool StandOnGround(IEllenseg ellenseg)
        {
            foreach (var item in this.EnemyInteractions(ellenseg))
            {
                if (ellenseg.Area.Bottom >= item.Area.Top)
                {
                    return true;
                }
            }

            return false;
        }

        private void ContactWhitPlayer(IEllenseg enemy)
        {
            if (enemy.Area.IntersectsWith(this.model.Jatekos.Area))
            {
                this.model.DeathNumbers++;
                this.PlayerDead?.Invoke();
            }
        }

        private List<IGameBlock> EnemyInteractions(IEllenseg enemy)
        {
            List<IGameBlock> interactItems = new List<IGameBlock>();
            foreach (var staticGameBlock in this.model.Gameblocks)
            {
                if (enemy.IsCollision(staticGameBlock))
                {
                    interactItems.Add(staticGameBlock);
                }
            }

            return interactItems;
        }

        private void Meghal(IEllenseg ellenseg)
        {
            this.model.Ellenseg.Remove(ellenseg as Ellenseg);
        }

        private void BossAttach()
        {
            if (Geometry.Combine(new RectangleGeometry(this.model.Jatekos.Area), this.model.Boss.Head, GeometryCombineMode.Intersect, null).GetArea() > 0)
            {
                this.PlayerDead?.Invoke();
            }
        }

        private void StoneBallAttack()
        {
            if (this.model.Boss.BallAttacksw.ElapsedMilliseconds > Config.StoneBallShotTime)
            {
                this.model.Boss.Attack = true;
                this.MonsterSound?.Invoke();
                this.model.Boss.CreateStoneBall();
                this.model.Boss.BallAttacksw.Restart();
            }

            if (this.model.Boss.StoneBall != null)
            {
                this.model.Boss.MoveStoneBallPos();
                if (Geometry.Combine(new RectangleGeometry(this.model.Jatekos.Area), this.model.Boss.StoneBall, GeometryCombineMode.Intersect, null).GetArea() > 0)
                {
                    this.model.DeathNumbers = this.model.DeathNumbers + 1;
                    this.PlayerDead?.Invoke();
                }
            }
        }

        private void KillPlayer()
        {
            if (Geometry.Combine(new RectangleGeometry(this.model.Jatekos.Area), this.model.Boss.Hand1, GeometryCombineMode.Intersect, null).GetArea() > 0
                || Geometry.Combine(new RectangleGeometry(this.model.Jatekos.Area), this.model.Boss.Hand2, GeometryCombineMode.Intersect, null).GetArea() > 0)
            {
                this.model.DeathNumbers = this.model.DeathNumbers + 1;
                this.PlayerDead?.Invoke();
            }
        }

        private void HandMove()
        {
            if (this.model.Boss.Hand1status == HandStatus.Hit)
            {
                this.model.Boss.Hand1 = this.model.Boss.NewHand1PosY(Config.MoveDownHand);
                this.Stand();
            }
            else if (this.model.Boss.Hand1status == HandStatus.Endhit)
            {
                this.model.Boss.Hand1 = this.model.Boss.NewHand1PosY(Config.MoveUpHand);
                if (this.model.Boss.Hand1.Bounds.Top < 100)
                {
                    this.model.Boss.Hand1status = HandStatus.Search;
                }
            }

            if (this.model.Boss.Hand2status == HandStatus.Hit)
            {
                this.model.Boss.Hand2 = this.model.Boss.NewHand2PosY(Config.MoveDownHand);
                this.Stand();
            }
            else if (this.model.Boss.Hand2status == HandStatus.Endhit)
            {
                this.model.Boss.Hand2 = this.model.Boss.NewHand2PosY(Config.MoveUpHand);
                if (this.model.Boss.Hand2.Bounds.Top < 100)
                {
                    this.model.Boss.Hand2status = HandStatus.Search;
                }
            }
        }

        private void Stand()
        {
            foreach (var item in this.model.Gameblocks)
            {
                if (Geometry.Combine(new RectangleGeometry(item.Area), this.model.Boss.Hand1, GeometryCombineMode.Intersect, null).GetArea() > 0)
                {
                    this.model.Boss.Hand1status = HandStatus.Endhit;
                }
            }

            foreach (var item in this.model.Gameblocks)
            {
                if (Geometry.Combine(new RectangleGeometry(item.Area), this.model.Boss.Hand2, GeometryCombineMode.Intersect, null).GetArea() > 0)
                {
                    this.model.Boss.Hand2status = HandStatus.Endhit;
                }
            }
        }

        private void Hand()
        {
            if (this.model.Boss.Sw1.ElapsedMilliseconds > 400)
            {
                this.model.Boss.Hand1status = HandStatus.Hit;
                this.model.Boss.Sw1.Reset();
            }
            else if (this.model.Boss.Sw2.ElapsedMilliseconds > 400)
            {
                this.model.Boss.Hand2status = HandStatus.Hit;
                this.model.Boss.Sw2.Reset();
            }
        }

        private void ChooseHand()
        {
            double tavolsag1 = 0;
            double tavolsag2 = 0;
            if (this.model.Boss.Hand1status == HandStatus.Search && this.model.Boss.Hand2status == HandStatus.Search)
            {
                tavolsag1 = Math.Abs(this.model.Boss.Hand1.Bounds.Left - this.model.Jatekos.Area.Left);
                tavolsag2 = Math.Abs(this.model.Boss.Hand2.Bounds.Left - this.model.Jatekos.Area.Left);
                if (tavolsag1 < tavolsag2)
                {
                    this.model.Boss.Hand1status = HandStatus.Wait;
                    this.model.Boss.Sw1.Start();
                    tavolsag1 = -(this.model.Boss.Hand1.Bounds.Left - this.model.Jatekos.Area.Left);
                    this.GoToX(tavolsag1);
                }
                else
                {
                    this.model.Boss.Hand2status = HandStatus.Wait;
                    this.model.Boss.Sw2.Start();
                    tavolsag2 = -(this.model.Boss.Hand2.Bounds.Left - this.model.Jatekos.Area.Left);
                    this.GoToX(tavolsag2);
                }
            }
            else if (this.model.Boss.Hand1status == HandStatus.Search && this.model.Boss.Hand2status == HandStatus.Endhit)
            {
                tavolsag1 = Math.Abs(this.model.Boss.Hand1.Bounds.Left - this.model.Jatekos.Area.Left);
                this.model.Boss.Hand1status = HandStatus.Wait;
                this.model.Boss.Sw1.Start();
                tavolsag1 = -(this.model.Boss.Hand1.Bounds.Left - this.model.Jatekos.Area.Left);
                this.GoToX(tavolsag1);
            }
            else if (this.model.Boss.Hand2status == HandStatus.Search && this.model.Boss.Hand1status == HandStatus.Endhit)
            {
                tavolsag2 = Math.Abs(this.model.Boss.Hand2.Bounds.Left - this.model.Jatekos.Area.Left);
                this.model.Boss.Hand2status = HandStatus.Wait;
                this.model.Boss.Sw2.Start();
                tavolsag2 = -(this.model.Boss.Hand2.Bounds.Left - this.model.Jatekos.Area.Left);
                this.GoToX(tavolsag2);
            }
        }

        private void GoToX(double tavolsag)
        {
            if (this.model.Boss.Hand1status == HandStatus.Wait)
            {
                this.model.Boss.Hand1 = this.model.Boss.NewHand1PosX(tavolsag);
            }
            else if (this.model.Boss.Hand2status == HandStatus.Wait)
            {
                this.model.Boss.Hand2 = this.model.Boss.NewHand2PosX(tavolsag);
            }
        }
    }
}
