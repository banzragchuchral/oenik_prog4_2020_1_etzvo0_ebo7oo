﻿// <copyright file="RepositoryLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using Model;
    using Repository;

    /// <summary>
    /// Logic class for repository.
    /// </summary>
    public class RepositoryLogic
    {
        private Repository rep = new Repository();

        /// <summary>
        /// Method for saving game.
        /// </summary>
        /// <param name="model">model.</param>
        /// <param name="time">time.</param>
        public void SaveGame(List<string> model, TimeSpan time)
        {
            model.Add(time.ToString());
            this.rep.Savegame(model);
        }

        /// <summary>
        /// Method for load game.
        /// </summary>
        /// <returns>string[].</returns>
        public string[] LoadGame()
        {
            return this.rep.Loadgame();
        }

        /// <summary>
        /// Method for getting if there is a saved game.
        /// </summary>
        /// <returns>bool.</returns>
        public bool GetboolSave()
        {
            return this.rep.GetboolSave();
        }
    }
}
