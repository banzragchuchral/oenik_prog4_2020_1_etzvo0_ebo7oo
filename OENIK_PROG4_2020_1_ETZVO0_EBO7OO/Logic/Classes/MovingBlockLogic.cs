﻿// <copyright file="MovingBlockLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Shapes;
    using Logic.Interfaces;
    using Model;

    /// <summary>
    /// Class for movingblock logic..
    /// </summary>
    public class MovingBlockLogic : IMovingBlockLogic
    {
        private IGameModel model;
        private IEllenseg dead;
        private List<IMovingGameblock> removeList;

        /// <summary>
        /// Initializes a new instance of the <see cref="MovingBlockLogic"/> class.
        /// </summary>
        /// <param name="model">model.</param>
        public MovingBlockLogic(IGameModel model)
        {
            this.model = model;
            this.removeList = new List<IMovingGameblock>();
        }

        /// <summary>
        /// Event for player dead.
        /// </summary>
        public event Hitenemy Enemyhit;

        /// <summary>
        /// Event for player dead.
        /// </summary>
        public event PlayerDeadEventHandler PlayerDead;

        /// <summary>
        /// Method for moving.
        /// </summary>
        public void Mozgas()
        {
            if (this.model.Jatekos.Playerbullets.Count() != 0)
            {
                foreach (var item in this.model.Jatekos.Playerbullets)
                {
                    this.BulletContactWhitEnemy(item);
                    if (this.model.Boss != null)
                    {
                        this.HitBoss(item);
                    }

                    this.BulletContactWhitOtherObject(item, true);
                    item.ChangeX(item.Dx);
                }
            }

            foreach (var cannon in this.model.Cannons)
            {
                this.CannonMove(cannon);
                if (cannon.Bullets.Count() != 0)
                {
                    foreach (var item in cannon.Bullets)
                    {
                        this.BulletContactWhitOtherObject(item, false);
                        item.ChangeX(item.Dx);
                        item.ChangeY(item.Dy);
                    }
                }
            }

            foreach (var item in this.model.Hats)
            {
                this.HatchetMove(item);
                Geometry g = new RectangleGeometry(this.model.Jatekos.Area);
                if (Geometry.Combine(g, item.Hatchetbody, GeometryCombineMode.Intersect, null).GetArea() > 0)
                {
                    this.model.DeathNumbers = this.model.DeathNumbers + 1;
                    this.PlayerDead?.Invoke();
                }
            }

            if (this.removeList.Count != 0)
            {
                this.model.Jatekos.Playerbullets.RemoveAll(x => this.removeList.Contains(x));
                foreach (var cannon in this.model.Cannons)
                {
                    cannon.Bullets.RemoveAll(x => this.removeList.Contains(x));
                }
            }
        }

        private void BulletContactWhitEnemy(IMovingGameblock bullet)
        {
            foreach (var item in this.model.Ellenseg)
            {
                if (item.Area.IntersectsWith(bullet.Area))
                {
                    this.Enemyhit?.Invoke();
                    this.dead = item;
                    this.removeList.Add(bullet);
                }
            }

            if (this.dead != null)
            {
                this.model.Ellenseg.Remove(this.dead as Ellenseg);
                this.dead = null;
            }
        }

        private void HitBoss(IMovingGameblock bullet)
        {
            if (Geometry.Combine(new RectangleGeometry(bullet.Area), this.model.Boss.Head, GeometryCombineMode.Intersect, null).GetArea() > 0)
            {
                this.Enemyhit?.Invoke();
                this.model.Boss.DamageBoss();
                this.removeList.Add(bullet);
            }
        }

        private void BulletContactWhitOtherObject(IMovingGameblock bullet, bool playerBullet)
        {
            if (bullet.Area.Top <= 0 || bullet.Area.Bottom >= this.model.GetHeight() || bullet.Area.Right >= this.model.GetWidth() || bullet.Area.Left <= 0)
            {
                this.removeList.Add(bullet);
            }

            foreach (var gameblock in this.model.Gameblocks)
            {
                if (bullet.IsCollision(gameblock))
                {
                    this.removeList.Add(bullet);
                }
            }

            if (this.model.Jatekos.IsCollision(bullet) && !playerBullet)
            {
                this.model.DeathNumbers = this.model.DeathNumbers + 1;
                this.PlayerDead?.Invoke();
            }
        }

        private void CannonShot(ICannon cannon)
        {
            if (cannon.Sw.ElapsedMilliseconds > cannon.TimeBetweenShotsForMiliseconds)
            {
                Vector diff;
                Vector d;
                if (cannon.Angle <= 0)
                {
                    diff = new Vector(cannon.ShotLine.Bounds.Location.X + cannon.ShotLine.Bounds.Size.Width, cannon.ShotLine.Bounds.Location.Y + cannon.ShotLine.Bounds.Size.Height);
                    d = new Vector(diff.X - cannon.RotatePoint.X, diff.Y - cannon.RotatePoint.Y);
                }
                else
                {
                    diff = new Vector(cannon.ShotLine.Bounds.Right - cannon.ShotLine.Bounds.Width, cannon.ShotLine.Bounds.Location.Y + cannon.ShotLine.Bounds.Size.Height);
                    d = new Vector(diff.X - cannon.RotatePoint.X, diff.Y - cannon.RotatePoint.Y);
                }

                if (cannon is SimpleCannon)
                {
                    cannon.Bullets.Add(new MovingGameblock(d.X / 10, d.Y / 10, Convert.ToInt32(diff.X), Convert.ToInt32(diff.Y), 8, 8));
                }
                else if (cannon is MarksManCannon)
                {
                    cannon.Bullets.Add(new MovingGameblock(d.X / 3, d.Y / 3, Convert.ToInt32(diff.X), Convert.ToInt32(diff.Y), 8, 8));
                }

                cannon.Sw.Restart();
            }
        }

        private void HatchetMove(IHatchet hatchet)
        {
            hatchet.Rotate();
        }

        private void CannonMove(ICannon cannon)
        {
            if (cannon is SimpleCannon)
            {
                this.CannonShot(cannon);
                cannon.Rotate();
            }
            else if (cannon is MarksManCannon)
            {
                this.AimingLineCalculat(cannon);
            }
            else if (cannon is LaserCannon)
            {
                this.LaserCalculat(cannon);
                cannon.Rotate();
            }
        }

        private void AimingLineCalculat(ICannon cannon)
        {
            Point playerpoz = new Point(this.model.Jatekos.Area.X + (this.model.Jatekos.Area.Width / 2), this.model.Jatekos.Area.Y);
            Point cannonpoz = new Point(cannon.RotatePoint.X, cannon.RotatePoint.Y + 10);
            Point realcannonpoz = new Point(cannon.RotatePoint.X, cannon.RotatePoint.Y);
            Geometry aiming = new LineGeometry(playerpoz, cannonpoz).GetWidenedPathGeometry(new Pen(Brushes.Black, 0.5));
            Geometry realaiming = new LineGeometry(playerpoz, realcannonpoz).GetWidenedPathGeometry(new Pen(Brushes.Black, 0.5));

            if (this.BlockAimingLine(aiming))
            {
                this.Aiming(cannon, aiming);
                this.CannonShot(cannon);
                if (cannon is MarksManCannon)
                {
                    (cannon as MarksManCannon).AimingLine = realaiming;
                }
            }
            else
            {
                cannon.Sw.Restart();
                cannon.Rotate();
                if (cannon is MarksManCannon)
                {
                    (cannon as MarksManCannon).AimingLine = null;
                }
            }
        }

        private bool BlockAimingLine(Geometry line)
        {
            foreach (var item in this.model.Gameblocks)
            {
                if (Geometry.Combine(line, new RectangleGeometry(item.Area), GeometryCombineMode.Intersect, null).GetArea() > 0)
                {
                    return false;
                }
            }

            return true;
        }

        private void Aiming(ICannon cannon, Geometry line)
        {
            int newAngle = 0;
            double i = 90;
            bool found = false;

            Geometry normal = new RectangleGeometry(new Rect(cannon.RotatePoint.X, cannon.RotatePoint.Y + 200, 5, 5));
            RotateTransform normalrotate = new RotateTransform(i, cannon.RotatePoint.X, cannon.RotatePoint.Y);

            if (this.model.Jatekos.Area.X >= cannon.RotatePoint.X)
            {
                i = 0;
            }

            while (i > -90 && !found)
            {
                normalrotate.Angle = i;
                normal.Transform = normalrotate;
                if (Geometry.Combine(normal, line, GeometryCombineMode.Intersect, null).GetArea() > 0)
                {
                    double s = 0;
                    if (cannon.Angle > i)
                    {
                        newAngle = Convert.ToInt32(i - 7);
                    }
                    else if (cannon.Angle <= i)
                    {
                        s = cannon.Angle + i - cannon.Angle;
                        newAngle = Convert.ToInt32(s + 7);
                    }

                    found = true;
                }

                i -= 1;
            }

            if (found)
            {
                cannon.AimingRotate(newAngle);
                cannon.Rotate();
            }
            else
            {
                cannon.Rotate();
            }
        }

        private void LaserCalculat(ICannon cannon)
        {
            Point rotPoint = new Point(cannon.RotatePoint.X, cannon.RotatePoint.Y);
            double xDistance = rotPoint.X;
            double yDistance = rotPoint.Y;
            double hDistance = this.model.GetHeight() - rotPoint.Y;
            double wDistance = this.model.GetWidth() - rotPoint.X;

            Vector diff;
            Vector d;

            if (cannon.Angle <= 0)
            {
                diff = new Vector(cannon.ShotLine.Bounds.Location.X + cannon.ShotLine.Bounds.Size.Width, cannon.ShotLine.Bounds.Location.Y + cannon.ShotLine.Bounds.Size.Height);
                d = new Vector(diff.X - cannon.RotatePoint.X, diff.Y - cannon.RotatePoint.Y);
            }
            else
            {
                diff = new Vector(cannon.ShotLine.Bounds.Right - cannon.ShotLine.Bounds.Width, cannon.ShotLine.Bounds.Location.Y + cannon.ShotLine.Bounds.Size.Height);
                d = new Vector(diff.X - cannon.RotatePoint.X, diff.Y - cannon.RotatePoint.Y);
            }

            if (diff.X < 0)
            {
                if (cannon.Angle > 45)
                {
                    double s = 0;
                    s = xDistance / d.X;
                    d.X *= s;
                    d.Y *= s;
                }
                else
                {
                    double s = 0;
                    s = hDistance / d.Y;
                    d.X *= s;
                    d.Y *= s;
                }
            }
            else if (diff.X >= 0)
            {
                if (cannon.Angle < -45)
                {
                    double s = 0;
                    s = wDistance / d.X;
                    d.X *= s;
                    d.Y *= s;
                }
                else
                {
                    double s = 0;
                    s = hDistance / d.Y;
                    d.X *= s;
                    d.Y *= s;
                }
            }

            Point endpoint = new Point(rotPoint.X + d.X, rotPoint.Y + d.Y);

            Geometry line = new LineGeometry(new Point(cannon.RotatePoint.X, cannon.RotatePoint.Y), new Point(endpoint.X, endpoint.Y))
                .GetWidenedPathGeometry(new Pen(Brushes.Black, 3));

            Gameblock ignore = null;
            foreach (var item in this.model.Gameblocks)
            {
                if (item.Area.Contains(cannon.RotatePoint))
                {
                    ignore = item;
                }
            }

            List<Gameblock> g = new List<Gameblock>();
            foreach (var item in this.model.Gameblocks)
            {
                if (Geometry.Combine(line, new RectangleGeometry(item.Area), GeometryCombineMode.Intersect, null).GetArea() > 0 && !item.Equals(ignore))
                {
                    g.Add(item);
                }
            }

            if (g.Count() != 0)
            {
                Gameblock e = g.First();

                double a = e.Area.Top;
                for (int i = 1; i < g.Count(); i++)
                {
                    double b = g[i].Area.Top;
                    if (a > b)
                    {
                        e = g[i];
                    }
                }

                Geometry levon = new RectangleGeometry(new Rect(
                    new Point(0, e.Area.Bottom),
                    new Point(this.model.GetWidth(), this.model.GetHeight())));

                line = Geometry.Combine(line, new RectangleGeometry(e.Area), GeometryCombineMode.Exclude, null);
                line = Geometry.Combine(line, levon, GeometryCombineMode.Exclude, null);

                if (cannon.Angle > 0)
                {
                    Geometry levon2 = new RectangleGeometry(new Rect(
                        new Point(e.Area.TopRight.X, e.Area.TopRight.Y),
                        new Point(0, this.model.GetHeight())));
                    line = Geometry.Combine(line, levon2, GeometryCombineMode.Exclude, null);
                }
                else if (cannon.Angle < 0)
                {
                    Geometry levon2 = new RectangleGeometry(new Rect(
                        new Point(e.Area.TopLeft.X, e.Area.TopLeft.Y),
                        new Point(this.model.GetWidth(), this.model.GetHeight())));
                    line = Geometry.Combine(line, levon2, GeometryCombineMode.Exclude, null);
                }

                (cannon as LaserCannon).AimingLine = line;
            }
            else
            {
                (cannon as LaserCannon).AimingLine = line;
            }

            if (Geometry.Combine(new RectangleGeometry(this.model.Jatekos.Area), line, GeometryCombineMode.Intersect, null).GetArea() > 0)
            {
                this.model.DeathNumbers = this.model.DeathNumbers + 1;
                this.PlayerDead?.Invoke();
            }
        }
    }
}