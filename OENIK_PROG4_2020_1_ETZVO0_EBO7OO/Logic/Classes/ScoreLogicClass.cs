﻿// <copyright file="ScoreLogicClass.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Score logic class.
    /// </summary>
    /// <typeparam name="T">T.</typeparam>
    public class ScoreLogicClass<T>
    {
        private Repository.HighscoreRepository<T> highscore = new Repository.HighscoreRepository<T>();

        /// <summary>
        /// Method for saving score.
        /// </summary>
        /// <param name="newsc">newsc.</param>
        public void SaveScore(T newsc)
        {
            this.highscore.AddHighscore(newsc);
        }

        /// <summary>
        /// Method for getting scores.
        /// </summary>
        /// <returns>list of string.</returns>
        public List<string> GetScores()
        {
            return this.highscore.LoadHighscore();
        }
    }
}
