﻿// <copyright file="PlayerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using Logic.Interfaces;
    using Model;

    /// <summary>
    /// This is the Logic for Player.
    /// </summary>
    public class PlayerLogic : IJatekosLogic
    {
        private IGameModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerLogic"/> class.
        /// </summary>
        /// <param name="model">model.</param>
        public PlayerLogic(IGameModel model)
        {
            this.model = model;

            // this.model.jatekos.DoubleJump = true;
            // this.model.jatekos.Jump = true;
        }

        /// <summary>
        /// Event handler for ending level.
        /// </summary>
        public event EndThisLevelEventHandler EndThisLevel;

        /// <summary>
        /// Event handler for player death.
        /// </summary>
        public event PlayerDeadEventHandler PlayerDead;

        /// <summary>
        /// Event for shooting.
        /// </summary>
        public event PlayerShoot Shoot;

        /// <summary>
        /// Event for jump;
        /// </summary>
        public event PlayerJump Jump;

        /// <summary>
        /// Method for shooting.
        /// </summary>
        public void Loves()
        {
            if (this.model.Jatekos.Playerbullets.Count < 3 && this.model.Jatekos.Sw.ElapsedMilliseconds > Config.Timebetweenplayershots)
            {
                this.Shoot?.Invoke();
            }

            if (this.model.Jatekos.Direction)
            {
                this.model.Jatekos.Loves(Config.PlayerBulletSpeed.X, Config.PlayerBulletSpeed.Y, this.model.Jatekos.Direction);
            }
            else
            {
                this.model.Jatekos.Loves(-Config.PlayerBulletSpeed.X, Config.PlayerBulletSpeed.Y, this.model.Jatekos.Direction);
            }
        }

        /// <summary>
        /// Method for jump.
        /// </summary>
        public void Ugras()
        {
            if (this.model.Jatekos.DoubleJump)
            {
                if (this.model.Jatekos.Dy > 0)
                {
                    this.model.Jatekos.Dy = 0;
                }

                this.model.Jatekos.Dy += Config.PlayerFirstJump;
                this.Jump?.Invoke();
                if (this.InteractCeiling())
                {
                    this.model.Jatekos.ChangeY(this.model.Jatekos.Dy);
                }
                else
                {
                    this.model.Jatekos.ChangeY(this.model.Jatekos.Dy);
                }

                this.model.Jatekos.DoubleJump = false;
            }
            else if (this.model.Jatekos.Jump)
            {
                if (this.model.Jatekos.Dy > 0)
                {
                    this.model.Jatekos.Dy = 0;
                }

                this.model.Jatekos.Dy += Config.PlayerSecondJump;
                this.Jump?.Invoke();
                if (this.InteractCeiling())
                {
                    this.model.Jatekos.ChangeY(this.model.Jatekos.Dy);
                }
                else
                {
                    this.model.Jatekos.ChangeY(this.model.Jatekos.Dy);
                }

                this.model.Jatekos.Jump = false;
            }
        }

        /// <summary>
        /// Method for moving.
        /// </summary>
        /// <param name="key">key.</param>
        public void Mozgas(Key key)
        {
            this.InteractWhitSpike();
            if (key == Key.D)
            {
                if (this.CanMoveRight())
                {
                    this.model.Jatekos.ChangeX(Config.PlayerMove.X);
                    this.model.Jatekos.Direction = true;
                }
            }

            if (key == Key.A)
            {
                if (this.CanMoveLeft())
                {
                    this.model.Jatekos.ChangeX(-Config.PlayerMove.X);
                    this.model.Jatekos.Direction = false;
                }
            }
        }

        /// <summary>
        /// Method for automove.
        /// </summary>
        public void AutoMovePlayer()
        {
            if (this.StandOnGround())
            {
                this.model.Jatekos.ChangeY(this.model.Jatekos.Dy);
                this.model.Jatekos.Dy = 0;
                this.model.Jatekos.Jump = true;
                this.model.Jatekos.DoubleJump = true;
            }
            else if (this.InteractCeiling())
            {
                this.model.Jatekos.ChangeY(this.model.Jatekos.Dy);
                this.model.Jatekos.Dy = 1;
            }
            else
            {
                if (this.model.Jatekos.Dy < Config.MaxFallenSpeed)
                {
                    this.model.Jatekos.Dy += Config.Graviti;
                }
            }

            this.InteractWhitSpike();
            this.OutTheMap();
            this.FinishTheLevel();
            this.model.Jatekos.ChangeY(this.model.Jatekos.Dy);
        }

        /// <summary>
        /// Method for player move.
        /// </summary>
        /// <param name="mozoga">mozoga.</param>
        /// <param name="mozogb">mozogb.</param>
        public void JatekosMozog(bool mozoga, bool mozogb)
        {
            if (mozoga == true || mozogb == true)
            {
                this.model.Jatekos.Mozog = true;
            }
            else
            {
                this.model.Jatekos.Mozog = false;
            }
        }

        private void OutTheMap()
        {
            if (this.model.GetHeight() <= this.model.Jatekos.Area.Top)
            {
                this.model.DeathNumbers = this.model.DeathNumbers + 1;
                this.PlayerDead?.Invoke();
            }
        }

        private void FinishTheLevel()
        {
            if (this.model.Jatekos.IsCollision(this.model.Endmapblock))
            {
                this.EndThisLevel?.Invoke();
            }
        }

        private bool InteractCeiling()
        {
            Rect nextArea = this.model.Jatekos.Area;
            nextArea.Y += this.model.Jatekos.Dy;
            foreach (var gameitem in this.PlayerInteractions(nextArea))
            {
                if (nextArea.Top <= gameitem.Area.Bottom && nextArea.Bottom > gameitem.Area.Bottom)
                {
                    if (nextArea.Right > gameitem.Area.Left && nextArea.Left < gameitem.Area.Right)
                    {
                        this.model.Jatekos.Dy = -(this.model.Jatekos.Area.Top - gameitem.Area.Bottom);
                        return true;
                    }
                }
            }

            return false;
        }

        private List<IGameBlock> PlayerInteractions()
        {
            List<IGameBlock> interactItems = new List<IGameBlock>();
            foreach (var staticGameBlock in this.model.Gameblocks)
            {
                if (this.model.Jatekos.IsCollision(staticGameBlock))
                {
                    interactItems.Add(staticGameBlock);
                }
            }

            return interactItems;
        }

        private List<IGameBlock> PlayerInteractions(Rect area)
        {
            List<IGameBlock> interactItems = new List<IGameBlock>();
            foreach (var staticGameBlock in this.model.Gameblocks)
            {
                if (staticGameBlock.Area.IntersectsWith(area))
                {
                    interactItems.Add(staticGameBlock);
                }
            }

            return interactItems;
        }

        private bool CanMoveRight()
        {
            Rect nextArea = this.model.Jatekos.Area;
            nextArea.X += 4;
            foreach (var item in this.PlayerInteractions(nextArea))
            {
                if (nextArea.Bottom > item.Area.Top && nextArea.Right >= item.Area.Left)
                {
                    this.model.Jatekos.ChangeX(item.Area.Left - this.model.Jatekos.Area.Right);
                    return false;
                }
            }

            if (nextArea.Right >= this.model.GetWidth())
            {
                return false;
            }

            return true;
        }

        private bool CanMoveLeft()
        {
            Rect nextArea = this.model.Jatekos.Area;
            nextArea.X -= 4;
            foreach (var item in this.PlayerInteractions(nextArea))
            {
                if (nextArea.Bottom > item.Area.Top && nextArea.Left <= item.Area.Right)
                {
                    this.model.Jatekos.ChangeX(item.Area.Right - this.model.Jatekos.Area.Left);
                    return false;
                }
            }

            if (nextArea.Left <= 0)
            {
                return false;
            }

            return true;
        }

        private bool StandOnGround()
        {
            Rect nextArea = this.model.Jatekos.Area;
            nextArea.Y += this.model.Jatekos.Dy;
            foreach (var item in this.PlayerInteractions(nextArea))
            {
                if (nextArea.Bottom >= item.Area.Top && nextArea.Top < item.Area.Top)
                {
                    if (nextArea.Right > item.Area.Left && nextArea.Left < item.Area.Right)
                    {
                        this.model.Jatekos.Dy = item.Area.Top - this.model.Jatekos.Area.Bottom;
                        return true;
                    }
                }
            }

            return false;
        }

        private void InteractWhitSpike()
        {
            foreach (var item in this.model.Spikes)
            {
                if (this.model.Jatekos.IsCollision(item))
                {
                    this.model.DeathNumbers = this.model.DeathNumbers + 1;
                    this.PlayerDead?.Invoke();
                }
            }
        }
    }
}
