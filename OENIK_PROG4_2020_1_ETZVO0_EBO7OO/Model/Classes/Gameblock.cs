﻿// <copyright file="Gameblock.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Holds the gameblock.
    /// </summary>
    public class Gameblock : IGameBlock
    {
        private Rect area;

        /// <summary>
        /// Initializes a new instance of the <see cref="Gameblock"/> class.
        /// </summary>
        /// <param name="x">x.</param>
        /// <param name="y">y.</param>
        /// <param name="w">w.</param>
        /// <param name="h">h.</param>
        public Gameblock(double x, double y, double w, double h)
        {
            this.area = new Rect(x, y, w, h);
        }

        /// <summary>
        /// Gets area of the gameblock.
        /// </summary>
        public Rect Area
        {
            get { return this.area; }
        }

        /// <summary>
        /// Checks if the object is intersects with other.
        /// </summary>
        /// <param name="other">gameblock.</param>
        /// <returns>bool.</returns>
        public bool IsCollision(IGameBlock other)
        {
            return this.Area.IntersectsWith(other.Area);
        }
    }
}
