﻿// <copyright file="Ellenseg.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Class for enemy object.
    /// </summary>
    public class Ellenseg : IEllenseg
    {
        private Rect area;

        /// <summary>
        /// Initializes a new instance of the <see cref="Ellenseg"/> class.
        /// </summary>
        /// <param name="eletero">eletero.</param>
        /// <param name="x">x.</param>
        /// <param name="y">y.</param>
        /// <param name="w">w.</param>
        /// <param name="h">h.</param>
        public Ellenseg(int eletero, double x, double y, double w, double h)
        {
            this.Brushes = new List<Brush>();
            this.Enemytempint = 1;
            this.area = new Rect(x, y, w, h);
            this.Eletero = eletero;
            this.DistanceTravelled = 0;

            for (int i = 1; i < 7; i++)
            {
                this.Brushes.Add(this.GetBursh("Model.Images.enemy" + i + ".png"));
            }
        }

        /// <summary>
        /// Gets or sets health point for enemy.
        /// </summary>
        public int Eletero { get; set; }

        /// <summary>
        /// Gets area of the object.
        /// </summary>
        public Rect Area
        {
            get { return this.area; }
        }

        /// <summary>
        /// Gets or sets vector x.
        /// </summary>
        public double Dx { get; set; }

        /// <summary>
        /// Gets or sets vector y.
        /// </summary>
        public double Dy { get; set; }

        /// <summary>
        /// Gets or sets range.
        /// </summary>
        public double Range { get; set; }

        /// <summary>
        /// Gets or sets distance travelled.
        /// </summary>
        public double DistanceTravelled { get; set; }

        /// <summary>
        /// Gets or sets enemytempint.
        /// </summary>
        public int Enemytempint { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether direction he looks.
        /// </summary>
        public bool Direction { get; set; }

        /// <summary>
        /// Gets or sets brushes.
        /// </summary>
        public List<Brush> Brushes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether dircetion he looks.
        /// </summary>
        public bool Fordul { get; set; }

        /// <summary>
        /// Checks if the object is intersects with other.
        /// </summary>
        /// <param name="other">gameblock.</param>
        /// <returns>bool.</returns>
        public bool IsCollision(IGameBlock other)
        {
            return this.Area.IntersectsWith(other.Area);
        }

        /// <summary>
        /// Method for chaning x.
        /// </summary>
        /// <param name="num">num.</param>
        public void ChangeX(double num)
        {
            if (num < 0)
            {
                this.Direction = false;
            }
            else
            {
                this.Direction = true;
            }

            this.area.X += num;
            this.DistanceTravelled += num;
        }

        /// <summary>
        /// Method for changing y.
        /// </summary>
        /// <param name="num">num.</param>
        public void ChangeY(double num)
        {
            this.area.Y += num;
            this.DistanceTravelled += num;
        }

        /// <summary>
        /// Method for changing x,y.
        /// </summary>
        /// <param name="numx">numx.</param>
        /// <param name="numy">numy.</param>
        public void SetXY(double numx, double numy)
        {
            this.area.X = numx;
            this.area.Y = numy;
        }

        private Brush GetBursh(string fname)
        {
            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
            bmp.EndInit();

            ImageBrush ib = new ImageBrush(bmp);

            return ib;
        }
    }
}
