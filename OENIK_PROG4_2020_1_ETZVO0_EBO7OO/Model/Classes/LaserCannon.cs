﻿// <copyright file="LaserCannon.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Class for laser cannon.
    /// </summary>
    public class LaserCannon : Cannon
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LaserCannon"/> class.
        /// </summary>
        /// <param name="x">x.</param>
        /// <param name="y">y.</param>
        /// <param name="w">width.</param>
        /// <param name="h">height.</param>
        /// <param name="shotTime">shottime.</param>
        public LaserCannon(int x, int y, int w, int h, int shotTime)
            : base(x, y, w, h, shotTime)
        {
        }

        /// <summary>
        /// Gets or sets the geometry of aimingline.
        /// </summary>
        public Geometry AimingLine { get; set; }

        /// <summary>
        /// Gets the cannon rotate speed.
        /// </summary>
        protected override double CannonRotateSpeed
        {
            get { return Config.LaserCannonRotateSpeed; }
        }

        /// <summary>
        /// Gets the cannon max rotate range.
        /// </summary>
        protected override double CannonMaxRotateRange
        {
            get { return Config.LaserCannonMaxRotateRange; }
        }

        /// <summary>
        /// Gets the cannon min rotate range.
        /// </summary>
        protected override double CannonMinRotateRange
        {
            get { return Config.LaserCannonMinRotateRange; }
        }

        /// <summary>
        /// Method for angle transformtaion.
        /// </summary>
        protected override void AngleTransform()
        {
            if (!this.ChangeDirection)
            {
                this.Angle += 0.3;
                if (this.Angle >= 45)
                {
                    this.ChangeDirection = true;
                }
            }
            else
            {
                this.Angle -= 0.3;
                if (this.Angle <= -45)
                {
                    this.ChangeDirection = false;
                }
            }
        }
    }
}
