﻿// <copyright file="Hatchet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using Model.Interfaces;

    /// <summary>
    /// Hatchet class.
    /// </summary>
    public abstract class Hatchet : IHatchet
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Hatchet"/> class.
        /// </summary>
        /// <param name="x">x.</param>
        /// <param name="y">y.</param>
        /// <param name="w">width.</param>
        /// <param name="h">height.</param>
        /// <param name="pixelratio">pixelratio.</param>
        public Hatchet(int x, int y, int w, int h, double pixelratio)
        {
            this.Hatchetbody = new GeometryGroup();
            this.Angle = 0;
        }

        /// <summary>
        /// Gets or sets a value indicating whether bool for changing dircetion.
        /// </summary>
        public bool ChangeDirection { get; set; }

        /// <summary>
        /// Gets or sets geomtery for hatchet body.
        /// </summary>
        public GeometryGroup Hatchetbody { get; set; }

        /// <summary>
        /// Gets or sets hatchet area.
        /// </summary>
        public Geometry Area { get; set; }

        /// <summary>
        /// Gets or sets angle of the transformation.
        /// </summary>
        public double Angle { get; set; }

        /// <summary>
        /// Gets or Sets the rotatepoint.
        /// </summary>
        public abstract Point RotatePoint { get; set; }

        /// <summary>
        /// Gets or sets the rotate transform.
        /// </summary>
        protected abstract RotateTransform RotateTransform { get; set; }

        /// <summary>
        /// Method for rotate.
        /// </summary>
        /// <returns>Geometry.</returns>
        public virtual Geometry Rotate()
        {
            this.Hatchetbody.Transform = this.RotateTransform;
            this.AngleTransform();
            this.RotateTransform.Angle = this.Angle;
            return this.Hatchetbody.GetFlattenedPathGeometry();
        }

        /// <summary>
        /// Method for angle transformation.
        /// </summary>
        protected virtual void AngleTransform()
        {
            if (!this.ChangeDirection)
            {
                this.Angle++;
                if (this.Angle >= 60)
                {
                    this.ChangeDirection = true;
                }
            }
            else
            {
                this.Angle--;
                if (this.Angle <= -60)
                {
                    this.ChangeDirection = false;
                }
            }
        }
    }
}
