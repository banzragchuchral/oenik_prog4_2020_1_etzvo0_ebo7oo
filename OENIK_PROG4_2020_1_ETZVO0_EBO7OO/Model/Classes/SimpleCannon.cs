﻿// <copyright file="SimpleCannon.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Class for simplecannon.
    /// </summary>
    public class SimpleCannon : Cannon
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleCannon"/> class.
        /// </summary>
        /// <param name="x">x.</param>
        /// <param name="y">y.</param>
        /// <param name="w">w.</param>
        /// <param name="h">h.</param>
        /// <param name="shotTime">shottime.</param>
        public SimpleCannon(int x, int y, int w, int h, int shotTime)
            : base(x, y, w, h, shotTime)
        {
        }

        /// <summary>
        /// Gets the rotate speed.
        /// </summary>
        protected override double CannonRotateSpeed
        {
            get { return Config.SimpleCannonRotateSpeed; }
        }

        /// <summary>
        /// Gets the max rotate range.
        /// </summary>
        protected override double CannonMaxRotateRange
        {
            get { return Config.SimpleCannonMaxRotateRange; }
        }

        /// <summary>
        /// Gets the min rotate range.
        /// </summary>
        protected override double CannonMinRotateRange
        {
            get { return Config.SimpleCannonMinRotateRange; }
        }

        /// <summary>
        /// Method for angel transformation.
        /// </summary>
        protected override void AngleTransform()
        {
            if (!this.ChangeDirection)
            {
                this.Angle += this.CannonRotateSpeed;
                if (this.Angle >= this.CannonMaxRotateRange)
                {
                    this.ChangeDirection = true;
                }
            }
            else
            {
                this.Angle -= this.CannonRotateSpeed;
                if (this.Angle <= this.CannonMinRotateRange)
                {
                    this.ChangeDirection = false;
                }
            }
        }
    }
}
