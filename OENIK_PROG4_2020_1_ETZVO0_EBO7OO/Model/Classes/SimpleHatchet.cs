﻿// <copyright file="SimpleHatchet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Class for simplehatchet.
    /// </summary>
    public class SimpleHatchet : Hatchet
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleHatchet"/> class.
        /// </summary>
        /// <param name="x">x.</param>
        /// <param name="y">y.</param>
        /// <param name="w">w.</param>
        /// <param name="h">h.</param>
        /// <param name="pixelratio">pixelratio.</param>
        public SimpleHatchet(int x, int y, int w, int h, double pixelratio)
            : base(x, y, w, h, pixelratio)
        {
            this.RotatePoint = new Point(x + (w / 2), y);
            this.RotateTransform = new RotateTransform(this.Angle, this.RotatePoint.X, this.RotatePoint.Y);
            this.Hatchetbody.Children.Add(new EllipseGeometry(new Point(this.RotatePoint.X, y + pixelratio + (0.375 * pixelratio)), pixelratio / 2, 0.375 * pixelratio));
            this.Hatchetbody.Children.Add(new RectangleGeometry(new Rect(this.RotatePoint.X, y, 6, h + (h * 3 / 40))));
            this.Area = new RectangleGeometry(new Rect(x, y, pixelratio, 1.75 * pixelratio));
        }

        /// <summary>
        /// Gets or sets the rotate pint.
        /// </summary>
        public override Point RotatePoint { get; set; }

        /// <summary>
        /// Gets or sets method for rotate transformation.
        /// </summary>
        protected override RotateTransform RotateTransform { get; set; }
    }
}
