﻿// <copyright file="MovingGameblock.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Class for moving game block.
    /// </summary>
    public class MovingGameblock : IMovingGameblock
    {
        private Rect area;

        /// <summary>
        /// Initializes a new instance of the <see cref="MovingGameblock"/> class.
        /// </summary>
        public MovingGameblock()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MovingGameblock"/> class.
        /// </summary>
        /// <param name="dx">dx.</param>
        /// <param name="dy">dy.</param>
        /// <param name="x">x.</param>
        /// <param name="y">y.</param>
        /// <param name="w">w.</param>
        /// <param name="h">h.</param>
        public MovingGameblock(double dx, double dy, int x, int y, int w, int h)
        {
            this.Dx = dx;
            this.Dy = dy;
            this.area = new Rect(x, y, w, h);
        }

        /// <summary>
        /// Gets or sets vector for x.
        /// </summary>
        public double Dx { get; set; }

        /// <summary>
        /// Gets or sets vector for y.
        /// </summary>
        public double Dy { get; set; }

        /// <summary>
        /// Gets area of the gameblock.
        /// </summary>
        public Rect Area
        {
            get { return this.area; }
        }

        /// <summary>
        /// Method for changing x.
        /// </summary>
        /// <param name="num">num.</param>
        public void ChangeX(double num)
        {
            this.area.X += num;
        }

        /// <summary>
        /// Method for changing y.
        /// </summary>
        /// <param name="num">num.</param>
        public void ChangeY(double num)
        {
            this.area.Y += num;
        }

        /// <summary>
        /// Checks if the object is intersects with other.
        /// </summary>
        /// <param name="other">other.</param>
        /// <returns>bool.</returns>
        public bool IsCollision(IGameBlock other)
        {
            return this.Area.IntersectsWith(other.Area);
        }

        /// <summary>
        /// Method for chagning x,y.
        /// </summary>
        /// <param name="numx">numx.</param>
        /// <param name="numy">numy.</param>
        public void SetXY(double numx, double numy)
        {
            this.area.X = numx;
            this.area.Y = numy;
        }
    }
}
