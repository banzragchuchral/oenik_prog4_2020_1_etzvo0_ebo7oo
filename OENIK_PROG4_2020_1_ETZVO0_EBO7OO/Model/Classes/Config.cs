﻿// <copyright file="Config.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Config static class for the game.
    /// </summary>
    public static class Config
    {
        /// <summary>
        /// Size of the player.
        /// </summary>
        private static double playerSize = 30;

        /// <summary>
        /// Size of the bullet.
        /// </summary>
        private static double bulletSize = 5;

        /// <summary>
        /// Rotate speed of the cannon.
        /// </summary>
        private static double simpleCannonRotateSpeed = 1;

        /// <summary>
        /// Maximum rotate range of cannon.
        /// </summary>
        private static double simpleCannonMaxRotateRange = 60;

        /// <summary>
        /// Minimum rotate range of cannon.
        /// </summary>
        private static double simpleCannonMinRotateRange = -60;

        /// <summary>
        /// Rotate speed of the marskman cannon.
        /// </summary>
        private static double marksManCannonRotateSpeed = 8;

        /// <summary>
        /// Maximum rotate range of the marskman cannon.
        /// </summary>
        private static double marksManCannonMaxRotateRange = 90;

        /// <summary>
        /// Minimum rotate range of the marskman cannon.
        /// </summary>
        private static double marksManCannonMinRotateRange = -90;

        /// <summary>
        /// Rotate speed of the laser cannon.
        /// </summary>
        private static double laserCannonRotateSpeed = 0.3;

        /// <summary>
        /// Maximum rotate range of the laser cannon.
        /// </summary>
        private static double laserCannonMaxRotateRange = 45;

        /// <summary>
        /// Minimum rotate range of the laser cannon.
        /// </summary>
        private static double laserCannonMinRotateRange = -45;

        /// <summary>
        /// Hatchet rotation speed.
        /// </summary>
        private static double hatchetRotationSpeed = 1;

        /// <summary>
        /// Hatchet max rotation range.
        /// </summary>
        private static double hatchetMaxRotationRange = 60;

        /// <summary>
        /// Hatchet minimum rotation range.
        /// </summary>
        private static double hatchetMinRotationRange = -60;

        /// <summary>
        /// Full circle hatchet rotation speed.
        /// </summary>
        private static double fullCircleHatchetRotationSpeed = 6;

        /// <summary>
        /// Player bullet speed.
        /// </summary>
        private static Vector playerBulletSpeed = new Vector(10, 0);

        /// <summary>
        /// Player movespeed.
        /// </summary>
        private static Vector playerMove = new Vector(6, 0);

        /// <summary>
        /// Player first jump vector.
        /// </summary>
        private static int playerFirstJump = -10;

        /// <summary>
        /// Player second jump vector.
        /// </summary>
        private static int playerSecondJump = -10;

        /// <summary>
        /// Graviti.
        /// </summary>
        private static int graviti = 1;

        /// <summary>
        /// Max falling speed.
        /// </summary>
        private static int maxFallenSpeed = 7;

        /// <summary>
        /// Enemy movespeed.
        /// </summary>
        private static Vector simpleEnemyMoveSpeed = new Vector(4, 0);

        /// <summary>
        /// Ball shot time.
        /// </summary>
        private static int stoneBallShotTime = 10000;

        /// <summary>
        /// Timer for hand down.
        /// </summary>
        private static double moveDownHand = 40;

        /// <summary>
        /// Timer for hand up.
        /// </summary>
        private static double moveUpHand = -12;

        /// <summary>
        /// Vector for stone ball.
        /// </summary>
        private static double moveStoneBall = -12;

        /// <summary>
        /// Time between player shots.
        /// </summary>
        private static int timebetweenplayershots = 1800;

        /// <summary>
        /// Gets or sets movestoneball.
        /// </summary>
        public static double MoveStoneBall { get => moveStoneBall; set => moveStoneBall = value; }

        /// <summary>
        /// Gets or sets moveuphand.
        /// </summary>
        public static double MoveUpHand { get => moveUpHand; set => moveUpHand = value; }

        /// <summary>
        /// Gets or sets movedownhand.
        /// </summary>
        public static double MoveDownHand { get => moveDownHand; set => moveDownHand = value; }

        /// <summary>
        /// Gets or sets stone ball shot time.
        /// </summary>
        public static int StoneBallShotTime { get => stoneBallShotTime; set => stoneBallShotTime = value; }

        /// <summary>
        /// Gets or sets enemy move speed.
        /// </summary>
        public static Vector SimpleEnemyMoveSpeed { get => simpleEnemyMoveSpeed; set => simpleEnemyMoveSpeed = value; }

        /// <summary>
        /// Gets or sets max fallen speed.
        /// </summary>
        public static int MaxFallenSpeed { get => maxFallenSpeed; set => maxFallenSpeed = value; }

        /// <summary>
        /// Gets or sets graviti.
        /// </summary>
        public static int Graviti { get => graviti; set => graviti = value; }

        /// <summary>
        /// Gets or sets player second jump.
        /// </summary>
        public static int PlayerSecondJump { get => playerSecondJump; set => playerSecondJump = value; }

        /// <summary>
        /// Gets or sets first jump.
        /// </summary>
        public static int PlayerFirstJump { get => playerFirstJump; set => playerFirstJump = value; }

        /// <summary>
        /// Gets or sets vector for player move.
        /// </summary>
        public static Vector PlayerMove { get => playerMove; set => playerMove = value; }

        /// <summary>
        /// Gets or sets bullet vector.
        /// </summary>
        public static Vector PlayerBulletSpeed { get => playerBulletSpeed; set => playerBulletSpeed = value; }

        /// <summary>
        /// Gets or sets circlehatchet rotation speed.
        /// </summary>
        public static double FullCircleHatchetRotationSpeed { get => fullCircleHatchetRotationSpeed; set => fullCircleHatchetRotationSpeed = value; }

        /// <summary>
        /// Gets or sets min rotate range.
        /// </summary>
        public static double HatchetMinRotationRange { get => hatchetMinRotationRange; set => hatchetMinRotationRange = value; }

        /// <summary>
        /// Gets or sets max rotate range.
        /// </summary>
        public static double HatchetMaxRotationRange { get => hatchetMaxRotationRange; set => hatchetMaxRotationRange = value; }

        /// <summary>
        /// Gets or sets hatchet rotation speed.
        /// </summary>
        public static double HatchetRotationSpeed { get => hatchetRotationSpeed; set => hatchetRotationSpeed = value; }

        /// <summary>
        /// Gets or sets laser cannon min rotate range.
        /// </summary>
        public static double LaserCannonMinRotateRange { get => laserCannonMinRotateRange; set => laserCannonMinRotateRange = value; }

        /// <summary>
        /// Gets or sets laser cannon max rotate range.
        /// </summary>
        public static double LaserCannonMaxRotateRange { get => laserCannonMaxRotateRange; set => laserCannonMaxRotateRange = value; }

        /// <summary>
        /// Gets or sets laser cannon rotate speed.
        /// </summary>
        public static double LaserCannonRotateSpeed { get => laserCannonRotateSpeed; set => laserCannonRotateSpeed = value; }

        /// <summary>
        /// Gets or sets marsman cannon min rotate range.
        /// </summary>
        public static double MarksManCannonMinRotateRange { get => marksManCannonMinRotateRange; set => marksManCannonMinRotateRange = value; }

        /// <summary>
        /// Gets or sets marsman cannon max rotate range.
        /// </summary>
        public static double MarksManCannonMaxRotateRange { get => marksManCannonMaxRotateRange; set => marksManCannonMaxRotateRange = value; }

        /// <summary>
        /// Gets or sets marsman cannon rotate speed.
        /// </summary>
        public static double MarksManCannonRotateSpeed { get => marksManCannonRotateSpeed; set => marksManCannonRotateSpeed = value; }

        /// <summary>
        /// Gets or sets simple cannon min rotate range.
        /// </summary>
        public static double SimpleCannonMinRotateRange { get => simpleCannonMinRotateRange; set => simpleCannonMinRotateRange = value; }

        /// <summary>
        /// Gets or sets simple cannon max rotate range.
        /// </summary>
        public static double SimpleCannonMaxRotateRange { get => simpleCannonMaxRotateRange; set => simpleCannonMaxRotateRange = value; }

        /// <summary>
        /// Gets or sets simple cannon rotate speed.
        /// </summary>
        public static double SimpleCannonRotateSpeed { get => simpleCannonRotateSpeed; set => simpleCannonRotateSpeed = value; }

        /// <summary>
        /// Gets or sets bullet size.
        /// </summary>
        public static double BulletSize { get => bulletSize; set => bulletSize = value; }

        /// <summary>
        /// Gets or sets player size.
        /// </summary>
        public static double PlayerSize { get => playerSize; set => playerSize = value; }

        /// <summary>
        /// Gets or sets time between player shots.
        /// </summary>
        public static int Timebetweenplayershots { get => timebetweenplayershots; set => timebetweenplayershots = value; }
    }
}
