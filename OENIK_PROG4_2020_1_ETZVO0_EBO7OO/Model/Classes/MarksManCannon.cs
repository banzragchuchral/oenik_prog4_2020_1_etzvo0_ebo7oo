﻿// <copyright file="MarksManCannon.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Class for marksman cannon.
    /// </summary>
    public class MarksManCannon : Cannon
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MarksManCannon"/> class.
        /// </summary>
        /// <param name="x">x.</param>
        /// <param name="y">y.</param>
        /// <param name="w">width.</param>
        /// <param name="h">height.</param>
        /// <param name="shotTime">shottime.</param>
        public MarksManCannon(int x, int y, int w, int h, int shotTime)
            : base(x, y, w, h, shotTime)
        {
        }

        /// <summary>
        /// Gets or sets the geometry of aimingline.
        /// </summary>
        public Geometry AimingLine { get; set; }

        /// <summary>
        /// Gets the cannon rotate speed.
        /// </summary>
        protected override double CannonRotateSpeed
        {
            get { return Config.MarksManCannonRotateSpeed; }
        }

        /// <summary>
        /// Gets the max rotate range of cannon.
        /// </summary>
        protected override double CannonMaxRotateRange
        {
            get { return Config.MarksManCannonMaxRotateRange; }
        }

        /// <summary>
        /// Gets the min rotate range of cannon.
        /// </summary>
        protected override double CannonMinRotateRange
        {
            get { return Config.MarksManCannonMinRotateRange; }
        }

        /// <summary>
        /// Set new angle.
        /// </summary>
        /// <param name="newAngle">newangle.</param>
        /// <returns>geometry.</returns>
        public override Geometry AimingRotate(int newAngle)
        {
            this.Angle = newAngle;
            return this.Area.GetFlattenedPathGeometry();
        }

        /// <summary>
        /// Method for angel transfromation.
        /// </summary>
        protected override void AngleTransform()
        {
            if (!this.ChangeDirection)
            {
                this.Angle += 8;
                if (this.Angle >= 90)
                {
                    this.ChangeDirection = true;
                }
            }
            else
            {
                this.Angle -= 8;
                if (this.Angle <= -90)
                {
                    this.ChangeDirection = false;
                }
            }
        }
    }
}
