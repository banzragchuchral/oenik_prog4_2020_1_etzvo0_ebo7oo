﻿// <copyright file="IMovingGameblock.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for moving game blocks.
    /// </summary>
    public interface IMovingGameblock : IGameBlock
    {
        /// <summary>
        /// Gets or sets vector for x.
        /// </summary>
        double Dx { get; set; }

        /// <summary>
        /// Gets or sets vector for y.
        /// </summary>
        double Dy { get; set; }

        /// <summary>
        /// Method for changing the x coordinate.
        /// </summary>
        /// <param name="num">num.</param>
        void ChangeX(double num);

        /// <summary>
        /// Method for changing the y. coordinate.
        /// </summary>
        /// <param name="num">num.</param>
        void ChangeY(double num);

        /// <summary>
        /// Method for changing the x and y coordinates.
        /// </summary>
        /// <param name="numx">numx.</param>
        /// <param name="numy">numy.</param>
        void SetXY(double numx, double numy);
    }
}
