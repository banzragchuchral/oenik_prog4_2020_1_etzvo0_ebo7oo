﻿// <copyright file="ICannon.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using Model.Interfaces;

    /// <summary>
    /// Interface for cannon.
    /// </summary>
    public interface ICannon : IRotation, IShoot
    {
        /// <summary>
        /// Gets or sets area.
        /// </summary>
        Geometry Area { get; set; }

        /// <summary>
        /// Gets or sets area2.
        /// </summary>
        Geometry Area2 { get; set; }

        /// <summary>
        /// Gets or sets shotline.
        /// </summary>
        LineGeometry ShotLine { get; set; }

        /// <summary>
        /// Method for rotation.
        /// </summary>
        /// <param name="newAngle">newangle.</param>
        /// <returns>geometry.</returns>
        Geometry AimingRotate(int newAngle);
    }
}
