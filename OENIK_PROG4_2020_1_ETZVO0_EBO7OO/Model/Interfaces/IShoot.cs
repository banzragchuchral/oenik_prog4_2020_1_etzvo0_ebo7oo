﻿// <copyright file="IShoot.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// All element interface that can shoot.
    /// </summary>
    public interface IShoot
    {
        /// <summary>
        /// Gets or sets this stopwatch measure the time between shots.
        /// </summary>
        Stopwatch Sw { get; set; }

        /// <summary>
        /// Gets or sets time that must elapse between two shots.
        /// </summary>
        int TimeBetweenShotsForMiliseconds { get; set; }

        /// <summary>
        /// Gets or sets all bullets that were fired.
        /// </summary>
        List<MovingGameblock> Bullets { get; set; }
    }
}
