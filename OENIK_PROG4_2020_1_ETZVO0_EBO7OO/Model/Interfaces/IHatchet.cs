﻿// <copyright file="IHatchet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using Model.Interfaces;

    /// <summary>
    /// Interface for hatchet.
    /// </summary>
    public interface IHatchet : IRotation
    {
        /// <summary>
        /// Gets hatchet body.
        /// </summary>
        GeometryGroup Hatchetbody { get; }

        /// <summary>
        /// Gets hatchet head.
        /// </summary>
        Geometry Area { get; }
    }
}
