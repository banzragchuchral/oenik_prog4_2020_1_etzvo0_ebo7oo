﻿// <copyright file="IRotation.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Interacae for rotation.
    /// </summary>
    public interface IRotation
    {
        /// <summary>
        /// Gets or sets the rotation point.
        /// </summary>
        Point RotatePoint { get; set; }

        /// <summary>
        /// Gets or sets the angle.
        /// </summary>
        double Angle { get; set; }

        /// <summary>
        /// Method for rotate.
        /// </summary>
        /// <returns>geometry.</returns>
        Geometry Rotate();
    }
}
