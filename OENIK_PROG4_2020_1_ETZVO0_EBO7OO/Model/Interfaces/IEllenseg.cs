﻿// <copyright file="IEllenseg.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for enemy objects.
    /// </summary>
    public interface IEllenseg : IGameBlock
    {
        /// <summary>
        /// Gets or sets health points for enemy.
        /// </summary>
        int Eletero { get; set; }

        /// <summary>
        /// Gets or sets vector for x.
        /// </summary>
        double Dx { get; set; }

        /// <summary>
        /// Gets or sets vector for y.
        /// </summary>
        double Dy { get; set; }

        /// <summary>
        /// Gets or sets range.
        /// </summary>
        double Range { get; set; }

        /// <summary>
        /// Gets or sets travelled distance.
        /// </summary>
        double DistanceTravelled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether direction look.
        /// </summary>
        bool Direction { get; set; }

        /// <summary>
        /// Method for chaning x.
        /// </summary>
        /// <param name="num">num.</param>
        void ChangeX(double num);

        /// <summary>
        /// Method for changing y.
        /// </summary>
        /// <param name="num">num.</param>
        void ChangeY(double num);

        /// <summary>
        /// Method for changing x,y.
        /// </summary>
        /// <param name="numx">numx.</param>
        /// <param name="numy">numy.</param>
        void SetXY(double numx, double numy);
    }
}
