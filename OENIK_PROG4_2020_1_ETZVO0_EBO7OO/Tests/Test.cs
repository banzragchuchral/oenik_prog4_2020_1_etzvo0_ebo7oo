﻿// <copyright file="Test.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using Logic;
    using Logic.Interfaces;
    using Model;
    using Model.Interfaces;
    using Moq;
    using NUnit;
    using NUnit.Framework;

    /// <summary>
    /// Test class.
    /// </summary>
    [TestFixture]
    public class Test
    {
        private IJatekosLogic playerlogic;
        private IEllensegLogic enemylogic;
        private IMovingBlockLogic movingblocklogic;
        private IGameModel model;

        /// <summary>
        /// Test initial.
        /// </summary>
        [SetUp]
        public void Init()
        {
            this.model = new GameModel(1920, 1080, 1);
            this.playerlogic = new PlayerLogic(this.model);
            this.enemylogic = new EnemyLogic(this.model);
            this.movingblocklogic = new MovingBlockLogic(this.model);
        }

        /// <summary>
        /// Test for play jump.
        /// </summary>
        [Test]
        public void Test1()
        {
            double a = this.model.Jatekos.Dy;
            this.playerlogic.Ugras();
            Assert.That(this.model.Jatekos.Dy != a);
        }

        /// <summary>
        /// Test for player move.
        /// </summary>
        [Test]
        public void Test2()
        {
            double a = this.model.Jatekos.Area.X;
            this.playerlogic.Mozgas(Key.D);
            Assert.That(this.model.Jatekos.Area.X != a);
            double b = this.model.Jatekos.Area.X;
            this.playerlogic.Mozgas(Key.F);
            Assert.That(this.model.Jatekos.Area.X == b);
        }

        /// <summary>
        /// Test for automoveplayer().
        /// </summary>
        [Test]
        public void Test3()
        {
            double a = this.model.Jatekos.Area.Y;
            this.playerlogic.AutoMovePlayer();
            Assert.That(this.model.Jatekos.Area.Y != a);
        }

        /// <summary>
        /// Test for enemy move.
        /// </summary>
        [Test]
        public void Test4()
        {
            double a = this.model.Ellenseg.First().Area.X;
            double b = this.model.Ellenseg.Last().Area.X;
            this.enemylogic.Mozgas();
            Assert.That(this.model.Ellenseg.First().Area.X != a);
            Assert.That(this.model.Ellenseg.Last().Area.X != b);
        }

        /// <summary>
        /// Test for moving block logic.
        /// </summary>
        [Test]
        public void Test5()
        {
            double a = this.model.Cannons.First().Angle;
            this.movingblocklogic.Mozgas();
            Assert.That(this.model.Cannons.First().Angle != a);
        }
    }
}
