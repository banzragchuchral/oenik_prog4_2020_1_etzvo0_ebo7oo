var namespace_model =
[
    [ "Classes", "namespace_model_1_1_classes.html", "namespace_model_1_1_classes" ],
    [ "Interfaces", "namespace_model_1_1_interfaces.html", "namespace_model_1_1_interfaces" ],
    [ "Cannon", "class_model_1_1_cannon.html", "class_model_1_1_cannon" ],
    [ "Ellenseg", "class_model_1_1_ellenseg.html", "class_model_1_1_ellenseg" ],
    [ "Gameblock", "class_model_1_1_gameblock.html", "class_model_1_1_gameblock" ],
    [ "GameModel", "class_model_1_1_game_model.html", "class_model_1_1_game_model" ],
    [ "Hatchet", "class_model_1_1_hatchet.html", "class_model_1_1_hatchet" ],
    [ "ICannon", "interface_model_1_1_i_cannon.html", "interface_model_1_1_i_cannon" ],
    [ "IEllenseg", "interface_model_1_1_i_ellenseg.html", "interface_model_1_1_i_ellenseg" ],
    [ "IGameBlock", "interface_model_1_1_i_game_block.html", "interface_model_1_1_i_game_block" ],
    [ "IGameModel", "interface_model_1_1_i_game_model.html", "interface_model_1_1_i_game_model" ],
    [ "IHatchet", "interface_model_1_1_i_hatchet.html", "interface_model_1_1_i_hatchet" ],
    [ "IJatekos", "interface_model_1_1_i_jatekos.html", "interface_model_1_1_i_jatekos" ],
    [ "IMovingGameblock", "interface_model_1_1_i_moving_gameblock.html", "interface_model_1_1_i_moving_gameblock" ],
    [ "IShoot", "interface_model_1_1_i_shoot.html", "interface_model_1_1_i_shoot" ],
    [ "Jatekos", "class_model_1_1_jatekos.html", "class_model_1_1_jatekos" ],
    [ "LaserCannon", "class_model_1_1_laser_cannon.html", "class_model_1_1_laser_cannon" ],
    [ "MarksManCannon", "class_model_1_1_marks_man_cannon.html", "class_model_1_1_marks_man_cannon" ],
    [ "MovingGameblock", "class_model_1_1_moving_gameblock.html", "class_model_1_1_moving_gameblock" ],
    [ "SimpleCannon", "class_model_1_1_simple_cannon.html", "class_model_1_1_simple_cannon" ],
    [ "Spike", "class_model_1_1_spike.html", "class_model_1_1_spike" ]
];