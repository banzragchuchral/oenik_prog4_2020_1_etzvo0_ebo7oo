var interface_model_1_1_i_jatekos =
[
    [ "ChangeX", "interface_model_1_1_i_jatekos.html#acabb5ce205fa6b60723dec0ea1fdf9da", null ],
    [ "ChangeY", "interface_model_1_1_i_jatekos.html#afc1b4af980dbc85671d264fb57cd6f1e", null ],
    [ "SetXY", "interface_model_1_1_i_jatekos.html#a9dd0740c645b0a8ccd22d8481148fceb", null ],
    [ "Direction", "interface_model_1_1_i_jatekos.html#a236d99bba3b9c225f5a996490bee149f", null ],
    [ "DoubleJump", "interface_model_1_1_i_jatekos.html#a23591564472f3a919dc9f7736bf4ea22", null ],
    [ "Dx", "interface_model_1_1_i_jatekos.html#aa6815f94e9ec38d73ee14b468811f63f", null ],
    [ "Dy", "interface_model_1_1_i_jatekos.html#aa8014849d4532c1fb0eba6a050892190", null ],
    [ "Jump", "interface_model_1_1_i_jatekos.html#a2a2b045a3a66a121af11008619a184d9", null ],
    [ "Sw", "interface_model_1_1_i_jatekos.html#aa3c41f4eb24dd9c7951a67563032bac2", null ]
];