var class_repository_1_1_highscore_repository =
[
    [ "HighscoreRepository", "class_repository_1_1_highscore_repository.html#a617595d18d612c08896131d547af157d", null ],
    [ "AddHighscore", "class_repository_1_1_highscore_repository.html#aa14c2c8e66767196aae1de05e8a7046f", null ],
    [ "LoadHighscore", "class_repository_1_1_highscore_repository.html#a1ca65b73c35bbb59a349938920880db3", null ],
    [ "SaveHighscore", "class_repository_1_1_highscore_repository.html#ab8ecb496d18ae47f211f6c78b20afbc9", null ],
    [ "Scores", "class_repository_1_1_highscore_repository.html#a7e7c133bb7c79ca191de5428fc0b2d3e", null ]
];