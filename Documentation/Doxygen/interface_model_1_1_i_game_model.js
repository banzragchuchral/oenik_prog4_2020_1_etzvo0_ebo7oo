var interface_model_1_1_i_game_model =
[
    [ "GetHeight", "interface_model_1_1_i_game_model.html#acc483d7a845ca42cc417041e6dab1079", null ],
    [ "GetWidth", "interface_model_1_1_i_game_model.html#ad97e924c13ed5973db0b56306ae12b5f", null ],
    [ "Boss", "interface_model_1_1_i_game_model.html#a8092d180fffa829eaabf7353b1bf38da", null ],
    [ "Cannons", "interface_model_1_1_i_game_model.html#a4825af2a01ffc3c8a22f6a1ea8cf4af7", null ],
    [ "DeathNumbers", "interface_model_1_1_i_game_model.html#a817269917d9391943ad8db48d9e05108", null ],
    [ "Ellenseg", "interface_model_1_1_i_game_model.html#a0584a04743b1f58bba81a8e3ca700e11", null ],
    [ "Endmapblock", "interface_model_1_1_i_game_model.html#a9f6e43d3b9567ebd730d452d7c4cfd18", null ],
    [ "Gameblocks", "interface_model_1_1_i_game_model.html#ac48972cfe7fdd895f80899b2725138ef", null ],
    [ "GameEnds", "interface_model_1_1_i_game_model.html#a543e1a8af649f7b7af973f40e1fdcd72", null ],
    [ "Hats", "interface_model_1_1_i_game_model.html#a6202c0d24731020cde93c7779898a215", null ],
    [ "Jatekos", "interface_model_1_1_i_game_model.html#ab65ed34b0de1309370fcc0cdf4cb3d90", null ],
    [ "Movgameblocks", "interface_model_1_1_i_game_model.html#a6c69f5554a6c751377c36ff56af27e57", null ],
    [ "Spikes", "interface_model_1_1_i_game_model.html#a61678315db1ffcddedeca2b4218bddec", null ]
];