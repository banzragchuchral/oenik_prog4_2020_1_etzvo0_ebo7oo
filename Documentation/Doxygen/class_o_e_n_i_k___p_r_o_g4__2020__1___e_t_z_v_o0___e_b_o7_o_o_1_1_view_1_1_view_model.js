var class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_view_model =
[
    [ "ViewModel", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_view_model.html#a12772fb294dc94a08ef2963d2935d02d", null ],
    [ "ExitCommand", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_view_model.html#a60c7414dd5c700e57bf725b2b5f53626", null ],
    [ "GameStartCommand", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_view_model.html#a0f3a18238185736867750add0a6b94e3", null ],
    [ "HighScoreCommand", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_view_model.html#a32fe8b5bfe91313d8ee1bf11e2621fdf", null ],
    [ "LoadStartCommand", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_view_model.html#ad045a456815761cf88a8922c8ca6107a", null ],
    [ "Score", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_view_model.html#ad8d0274921dba4d9041c244ef9f3922a", null ],
    [ "Scores", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_view_model.html#aa52b518841e506a8faf3edd316c3ed0c", null ],
    [ "SwitchView", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_view_model.html#ac9607b591464714c39faba21d72e4335", null ],
    [ "Tempscore", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_view_model.html#a4a3b00db8d1c424f6e430cc8b803db84", null ]
];