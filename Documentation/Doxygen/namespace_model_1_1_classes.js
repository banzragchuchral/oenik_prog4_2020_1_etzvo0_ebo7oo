var namespace_model_1_1_classes =
[
    [ "Boss", "class_model_1_1_classes_1_1_boss.html", "class_model_1_1_classes_1_1_boss" ],
    [ "FastHatchet", "class_model_1_1_classes_1_1_fast_hatchet.html", "class_model_1_1_classes_1_1_fast_hatchet" ],
    [ "FullCircleHatchet", "class_model_1_1_classes_1_1_full_circle_hatchet.html", "class_model_1_1_classes_1_1_full_circle_hatchet" ],
    [ "SimpleHatchet", "class_model_1_1_classes_1_1_simple_hatchet.html", "class_model_1_1_classes_1_1_simple_hatchet" ]
];