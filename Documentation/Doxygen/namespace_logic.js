var namespace_logic =
[
    [ "Interfaces", "namespace_logic_1_1_interfaces.html", "namespace_logic_1_1_interfaces" ],
    [ "EnemyLogic", "class_logic_1_1_enemy_logic.html", "class_logic_1_1_enemy_logic" ],
    [ "IEllensegLogic", "interface_logic_1_1_i_ellenseg_logic.html", "interface_logic_1_1_i_ellenseg_logic" ],
    [ "IJatekosLogic", "interface_logic_1_1_i_jatekos_logic.html", "interface_logic_1_1_i_jatekos_logic" ],
    [ "IMovingBlockLogic", "interface_logic_1_1_i_moving_block_logic.html", "interface_logic_1_1_i_moving_block_logic" ],
    [ "MovingBlockLogic", "class_logic_1_1_moving_block_logic.html", "class_logic_1_1_moving_block_logic" ],
    [ "PlayerLogic", "class_logic_1_1_player_logic.html", "class_logic_1_1_player_logic" ],
    [ "RepositoryLogic", "class_logic_1_1_repository_logic.html", "class_logic_1_1_repository_logic" ],
    [ "ScoreLogicClass", "class_logic_1_1_score_logic_class.html", "class_logic_1_1_score_logic_class" ]
];