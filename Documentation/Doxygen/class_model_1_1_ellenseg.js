var class_model_1_1_ellenseg =
[
    [ "Ellenseg", "class_model_1_1_ellenseg.html#ab2929a620c7c4818f8c69e945c0d4d5e", null ],
    [ "ChangeX", "class_model_1_1_ellenseg.html#a9f974fe3988a149cf3db4061e6bbf50a", null ],
    [ "ChangeY", "class_model_1_1_ellenseg.html#a6a65099a7cd628f7c5072398a0846a36", null ],
    [ "IsCollision", "class_model_1_1_ellenseg.html#a0e8d92e6938c4e25d626048ff9322613", null ],
    [ "SetXY", "class_model_1_1_ellenseg.html#ac793beeda669517d51e165a1768214bd", null ],
    [ "Area", "class_model_1_1_ellenseg.html#a4332405d66d3517be4f0b09825578ead", null ],
    [ "Brushes", "class_model_1_1_ellenseg.html#ac100ebcef210fd4ccf5615909206b9d2", null ],
    [ "Direction", "class_model_1_1_ellenseg.html#a433b1883dc584c15d101adb6b549d38f", null ],
    [ "DistanceTravelled", "class_model_1_1_ellenseg.html#ad67d5cc28687a2ea567321a7dededad6", null ],
    [ "Dx", "class_model_1_1_ellenseg.html#a1ed243ca773d3335aceaef0aab18ab06", null ],
    [ "Dy", "class_model_1_1_ellenseg.html#a12c24fe4b081e4939a71cad6ba50c19f", null ],
    [ "Eletero", "class_model_1_1_ellenseg.html#af9de4c7057abf4e59ed3cda103b9730b", null ],
    [ "Enemytempint", "class_model_1_1_ellenseg.html#abad0c41022b6c1a0584a8a12ed767c1f", null ],
    [ "Fordul", "class_model_1_1_ellenseg.html#a7637b70083a1db068ac6b15a7a737210", null ],
    [ "Range", "class_model_1_1_ellenseg.html#ac3b6455e043a62c70877f6885dcf39c7", null ]
];