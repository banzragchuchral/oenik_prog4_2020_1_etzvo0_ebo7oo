var searchData=
[
  ['ballattacksw_11',['BallAttacksw',['../class_model_1_1_classes_1_1_boss.html#ad9397eae23638bb580d65babe31ea24a',1,'Model::Classes::Boss']]],
  ['boss_12',['Boss',['../class_model_1_1_classes_1_1_boss.html',1,'Model.Classes.Boss'],['../class_model_1_1_game_model.html#aecd7f452880a2439c0d27f7ded1a30aa',1,'Model.GameModel.Boss()'],['../interface_model_1_1_i_game_model.html#a8092d180fffa829eaabf7353b1bf38da',1,'Model.IGameModel.Boss()'],['../class_model_1_1_classes_1_1_boss.html#ac9ff68c852ba14ed3d89024924ecc922',1,'Model.Classes.Boss.Boss()']]],
  ['bossattack_13',['BossAttack',['../class_logic_1_1_enemy_logic.html#af827ca80598cec5307eabd5bcf380222',1,'Logic.EnemyLogic.BossAttack()'],['../interface_logic_1_1_i_ellenseg_logic.html#a026a9708a87f5dfeb76d4c5f22a417de',1,'Logic.IEllensegLogic.BossAttack()']]],
  ['brushes_14',['Brushes',['../class_model_1_1_ellenseg.html#ac100ebcef210fd4ccf5615909206b9d2',1,'Model::Ellenseg']]],
  ['bullets_15',['Bullets',['../class_model_1_1_cannon.html#a6ed5c680c3ca1a4f9f09832d272ecaa4',1,'Model.Cannon.Bullets()'],['../interface_model_1_1_i_shoot.html#af340985563ebac7690104f68aee878cb',1,'Model.IShoot.Bullets()']]]
];
