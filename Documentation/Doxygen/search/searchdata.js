var indexSectionsWithContent =
{
  0: "abcdefghijklmnoprstuvwx",
  1: "abcefghijlmprstv",
  2: "lmortx",
  3: "abcdefghijklmnoprstuv",
  4: "h",
  5: "ehsw",
  6: "abcdefghjklmnprstu",
  7: "ejmps",
  8: "cln"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "enums",
  5: "enumvalues",
  6: "properties",
  7: "events",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Enumerations",
  5: "Enumerator",
  6: "Properties",
  7: "Events",
  8: "Pages"
};

