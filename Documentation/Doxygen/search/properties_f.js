var searchData=
[
  ['savedtime_414',['SavedTime',['../class_model_1_1_game_model.html#a2b802f1a89e4190fa64832713a5d5440',1,'Model::GameModel']]],
  ['score_415',['Score',['../class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_view_model.html#ad8d0274921dba4d9041c244ef9f3922a',1,'OENIK_PROG4_2020_1_ETZVO0_EBO7OO::View::ViewModel']]],
  ['scores_416',['Scores',['../class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_view_model.html#aa52b518841e506a8faf3edd316c3ed0c',1,'OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View.ViewModel.Scores()'],['../class_repository_1_1_highscore_repository.html#a7e7c133bb7c79ca191de5428fc0b2d3e',1,'Repository.HighscoreRepository.Scores()']]],
  ['shotline_417',['ShotLine',['../class_model_1_1_cannon.html#a3be4fad9a894eb71d08fee4030a307f6',1,'Model.Cannon.ShotLine()'],['../interface_model_1_1_i_cannon.html#a31315ab468c28141aaac41739eb943f1',1,'Model.ICannon.ShotLine()']]],
  ['spikes_418',['Spikes',['../class_model_1_1_game_model.html#a08d5b8b1a5394937d5c3c5051729af7e',1,'Model.GameModel.Spikes()'],['../interface_model_1_1_i_game_model.html#a61678315db1ffcddedeca2b4218bddec',1,'Model.IGameModel.Spikes()']]],
  ['stoneball_419',['StoneBall',['../class_model_1_1_classes_1_1_boss.html#a2738eb4d8e83deae338b877e5d21ddb8',1,'Model::Classes::Boss']]],
  ['sw_420',['Sw',['../class_model_1_1_cannon.html#a351e2629b4b7f7a66f473fba0831a56a',1,'Model.Cannon.Sw()'],['../class_model_1_1_jatekos.html#acb67b1b5a4d1ce62d2ae9cb7df73e18c',1,'Model.Jatekos.Sw()'],['../interface_model_1_1_i_jatekos.html#aa3c41f4eb24dd9c7951a67563032bac2',1,'Model.IJatekos.Sw()'],['../interface_model_1_1_i_shoot.html#a3ff65dc00cd110fae5ca9c9e1885a7f2',1,'Model.IShoot.Sw()']]],
  ['sw1_421',['Sw1',['../class_model_1_1_classes_1_1_boss.html#afc0122e12abf2ea2c80fb626d80ae5db',1,'Model::Classes::Boss']]],
  ['sw2_422',['Sw2',['../class_model_1_1_classes_1_1_boss.html#a10a289d4ec7952185d471e9ddafcd26e',1,'Model::Classes::Boss']]],
  ['switchview_423',['SwitchView',['../class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_view_model.html#ac9607b591464714c39faba21d72e4335',1,'OENIK_PROG4_2020_1_ETZVO0_EBO7OO::View::ViewModel']]]
];
