var interface_model_1_1_i_ellenseg =
[
    [ "ChangeX", "interface_model_1_1_i_ellenseg.html#a878bd13fea4a57e984e1ad7212b68827", null ],
    [ "ChangeY", "interface_model_1_1_i_ellenseg.html#ad7b529edea43823a4c90f373f8df14bf", null ],
    [ "SetXY", "interface_model_1_1_i_ellenseg.html#ab193324168481f9f19d1254320f02a32", null ],
    [ "Direction", "interface_model_1_1_i_ellenseg.html#a4134611f580ee3d242d26880fb4dd857", null ],
    [ "DistanceTravelled", "interface_model_1_1_i_ellenseg.html#a6b658b7b37f53831e28d87b4b5b53acb", null ],
    [ "Dx", "interface_model_1_1_i_ellenseg.html#a520936588bcb7974333948beb2cbd42e", null ],
    [ "Dy", "interface_model_1_1_i_ellenseg.html#a18bb61b6ca554a8f877a47fa3cd4fbc8", null ],
    [ "Eletero", "interface_model_1_1_i_ellenseg.html#a293627e8cdb86a76a8670d7c10632edb", null ],
    [ "Range", "interface_model_1_1_i_ellenseg.html#af74238b5835e7fbdfa4dfa5e44fdb4bd", null ]
];