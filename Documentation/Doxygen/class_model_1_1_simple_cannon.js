var class_model_1_1_simple_cannon =
[
    [ "SimpleCannon", "class_model_1_1_simple_cannon.html#a027ccbbf53e13d9f459dc99160a639bb", null ],
    [ "AngleTransform", "class_model_1_1_simple_cannon.html#a352c5262929a09fe154fdebffd6d7fab", null ],
    [ "CannonMaxRotateRange", "class_model_1_1_simple_cannon.html#ad388e0082d4ed0fb7f9ec410653212aa", null ],
    [ "CannonMinRotateRange", "class_model_1_1_simple_cannon.html#a0cbbdbff2cc61d0b76fc46053688a68e", null ],
    [ "CannonRotateSpeed", "class_model_1_1_simple_cannon.html#ae35acbcfefbde3e4cb2a408a83fd523c", null ]
];