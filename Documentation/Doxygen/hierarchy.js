var hierarchy =
[
    [ "Application", null, [
      [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.App", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_app.html", null ]
    ] ],
    [ "FrameworkElement", null, [
      [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.GameControl", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_game_control.html", null ]
    ] ],
    [ "Repository.HighscoreRepository< T >", "class_repository_1_1_highscore_repository.html", null ],
    [ "Model.Interfaces.IBoss", "interface_model_1_1_interfaces_1_1_i_boss.html", [
      [ "Model.Classes.Boss", "class_model_1_1_classes_1_1_boss.html", null ]
    ] ],
    [ "IComponentConnector", null, [
      [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.GameWindow", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_game_window.html", null ],
      [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View.Exitmenu", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_exitmenu.html", null ],
      [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View.Game", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_game.html", null ],
      [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View.Gameend", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_gameend.html", null ],
      [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View.Highscore", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_highscore.html", null ],
      [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View.Menu", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_menu.html", null ]
    ] ],
    [ "Model.IGameBlock", "interface_model_1_1_i_game_block.html", [
      [ "Model.Gameblock", "class_model_1_1_gameblock.html", null ],
      [ "Model.IEllenseg", "interface_model_1_1_i_ellenseg.html", [
        [ "Model.Ellenseg", "class_model_1_1_ellenseg.html", null ]
      ] ],
      [ "Model.IJatekos", "interface_model_1_1_i_jatekos.html", [
        [ "Model.Jatekos", "class_model_1_1_jatekos.html", null ]
      ] ],
      [ "Model.IMovingGameblock", "interface_model_1_1_i_moving_gameblock.html", [
        [ "Model.MovingGameblock", "class_model_1_1_moving_gameblock.html", null ]
      ] ],
      [ "Model.Spike", "class_model_1_1_spike.html", null ]
    ] ],
    [ "Model.IGameModel", "interface_model_1_1_i_game_model.html", [
      [ "Model.GameModel", "class_model_1_1_game_model.html", null ]
    ] ],
    [ "Logic.Interfaces.ILogic", "interface_logic_1_1_interfaces_1_1_i_logic.html", [
      [ "Logic.IEllensegLogic", "interface_logic_1_1_i_ellenseg_logic.html", [
        [ "Logic.EnemyLogic", "class_logic_1_1_enemy_logic.html", null ]
      ] ],
      [ "Logic.IJatekosLogic", "interface_logic_1_1_i_jatekos_logic.html", [
        [ "Logic.PlayerLogic", "class_logic_1_1_player_logic.html", null ]
      ] ],
      [ "Logic.IMovingBlockLogic", "interface_logic_1_1_i_moving_block_logic.html", [
        [ "Logic.MovingBlockLogic", "class_logic_1_1_moving_block_logic.html", null ]
      ] ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Model.Interfaces.IRotation", "interface_model_1_1_interfaces_1_1_i_rotation.html", [
      [ "Model.ICannon", "interface_model_1_1_i_cannon.html", [
        [ "Model.Cannon", "class_model_1_1_cannon.html", [
          [ "Model.LaserCannon", "class_model_1_1_laser_cannon.html", null ],
          [ "Model.MarksManCannon", "class_model_1_1_marks_man_cannon.html", null ],
          [ "Model.SimpleCannon", "class_model_1_1_simple_cannon.html", null ]
        ] ]
      ] ],
      [ "Model.IHatchet", "interface_model_1_1_i_hatchet.html", [
        [ "Model.Hatchet", "class_model_1_1_hatchet.html", [
          [ "Model.Classes.FastHatchet", "class_model_1_1_classes_1_1_fast_hatchet.html", null ],
          [ "Model.Classes.FullCircleHatchet", "class_model_1_1_classes_1_1_full_circle_hatchet.html", null ],
          [ "Model.Classes.SimpleHatchet", "class_model_1_1_classes_1_1_simple_hatchet.html", null ]
        ] ]
      ] ]
    ] ],
    [ "Model.IShoot", "interface_model_1_1_i_shoot.html", [
      [ "Model.ICannon", "interface_model_1_1_i_cannon.html", null ]
    ] ],
    [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Renderer", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_renderer.html", null ],
    [ "Repository.Repository", "class_repository_1_1_repository.html", null ],
    [ "Logic.RepositoryLogic", "class_logic_1_1_repository_logic.html", null ],
    [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View.ScoreClass", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_score_class.html", null ],
    [ "Logic.ScoreLogicClass< T >", "class_logic_1_1_score_logic_class.html", null ],
    [ "Tests.Test", "class_tests_1_1_test.html", null ],
    [ "UserControl", null, [
      [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View.Game", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_game.html", null ],
      [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View.Menu", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_menu.html", null ]
    ] ],
    [ "ViewModelBase", null, [
      [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View.ViewModel", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_view_model.html", null ]
    ] ],
    [ "Window", null, [
      [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.GameWindow", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_game_window.html", null ],
      [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.MainWindow", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_main_window.html", null ],
      [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View.Exitmenu", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_exitmenu.html", null ],
      [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View.Gameend", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_gameend.html", null ],
      [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View.Highscore", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_highscore.html", null ],
      [ "OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View.MainMenu", "class_o_e_n_i_k___p_r_o_g4__2020__1___e_t_z_v_o0___e_b_o7_o_o_1_1_view_1_1_main_menu.html", null ]
    ] ]
];